const app = require('express')()
const http = require('http').createServer(app)
const io = require('socket.io')(http);
const PORT = process.env.PORT||3231

var sockets = {};   /// users connected to server via apps/browsers

function getChatSocket(socket){
	console.log(sockets)
    return socket.to(sockets["chat-user-"+socket]);
}
 
 
function setChatSocket(socket, data){
    sockets["chat-user-"+socket] = data;
}
 
function deleteChatSocket(socket){
    delete sockets["chat-user-"+socket];
}
io.on('connection', function(socket){
    socket.on('come_online', function (data) {
		console.log(data)
        socket.soketid = data.user_id;
        setChatSocket(socket.soketid, socket);
	});
        
	socket.on('disconnect', function() {
        var userId = socket.soketid;
        deleteChatSocket(socket.soketid);
          
    });
 
    socket.on('sendmessage', function (data) {
		console.log(sockets["chat-user-"+data.receiver_id])
		//  console.log(sockets["chat-user-"+data.user.receiver_id].conn.id)
		// if(sockets["chat-user-"+data.user.receiver_id].soketid==data.user.receiver_id)
		// io.emit('send_message',data)
		// if()
		if(sockets["chat-user-"+data.receiver_id]){
			socket.to(sockets["chat-user-"+data.receiver_id].conn.id).emit('send_message',data);
		}
     
    });
});
http.listen(PORT,()=>{
    console.log("Connected to Port: "+PORT)
})