<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    "prefix" => "v1",
    "middleware" => "auth:api",
], function () {

    Route::group([
        "prefix" => "users",
    ], function () {
        Route::get("total_order", "UserController@total_order");
        Route::get("/", "UserController@index");
        Route::get("getProductRate", "UserController@getProductRate");
        Route::get("getReviews", "UserController@getReviews");
        Route::post("activity", "UserController@activity");
        Route::post("notification", "UserController@notification");
        Route::post("create", "UserController@create");
        Route::post("update", "UserController@update");
        Route::post('delete', 'UserController@delete');
        Route::post('getmessages', 'UserController@getmessages');
        Route::post('reviews', 'UserController@reviews');
        Route::post('location', 'UserController@location');
        Route::post('products', 'UserController@products');
        Route::post('saved_products', 'UserController@saved_products');
        Route::post('reports', 'UserController@reports');
        Route::post('product_category', 'UserController@product_category');
        Route::post('reservation', 'UserController@reservation');
        Route::post('approval', 'UserController@approval');
        Route::post('messages', 'UserController@messages');
        Route::post('productReviews', 'UserController@productReviews');
        Route::post("search", "UserController@searchProduct");
        Route::post('followers', 'UserController@followers');
        Route::post('you_follow', 'UserController@you_follow');
        Route::get('hasproduct', 'UserController@hasproduct');
        Route::post('set_category_id', 'UserController@set_category_id');
        Route::post("getUserRating", "UserController@getUserRating");
        Route::post("getNotif", "UserController@getNotif");
        Route::post("getContact", "UserController@getContact");
        Route::post("setContactApproval", "UserController@setContactApproval");
        Route::post("getApprovedRequested", "UserController@getApprovedRequested");
        Route::get("getUserInfo", "UserController@getUserInfo");
        Route::post("deactivate", "UserController@deactivate");

    });


    Route::group([
        "prefix" => "messages",
    ], function () {

        Route::get("/", "MessagesController@index");
        Route::post("create", "MessagesController@create");
        Route::post("update", "MessagesController@update");
        Route::post("updateSeen", "MessagesController@updateSeen");
        Route::post('delete', 'MessagesController@delete');
        Route::post('deleteAll', 'MessagesController@deleteAll');
        Route::post('deleteMessagebyid', 'MessagesController@deleteMessagebyid');

    });


    Route::group([
        "prefix" => "products",
    ], function () {

        Route::get("/", "ProductController@index");
        Route::post("create", "ProductController@create");
        Route::post("update", "ProductController@update");
        Route::post('delete', 'ProductController@delete');
        Route::post('productReviews', 'ProductController@productReviews');
        Route::get('maxProducts', 'ProductController@maxProducts');
        Route:: post('restore', 'ProductController@restore');
        Route::post('getProductContact', 'ProductController@getProductContact');
        Route:: post('setProductContactApproval', 'ProductController@setProductContactApproval');
        //this will be used if already have a contact
        Route::post('setApproveRequestContact', 'ProductController@setApproveRequestContact');
        Route::post('getDistanceViaLatLong', 'ProductController@getDistanceViaLatLong');

    });

    Route::group([
        "prefix" => "category",
    ], function () {

        Route::get("/", "ProductCategoryController@index");
        Route::post("create", "ProductCategoryController@create");
        Route::post("update", "ProductCategoryController@update");
        Route::post('delete', 'ProductCategoryController@delete');
        Route::get('productCategory', 'ProductCategoryController@productCategory');
        Route::post('productCategoryfilter', 'ProductCategoryController@productCategoryfilter');

    });

    Route::group([
        "prefix" => "locations",
    ], function () {

        Route::get("/", "LocationController@index");
        Route::post("create", "LocationController@create");
        Route::post("update", "LocationController@update");
        Route::post('delete', 'LocationController@delete');

    });

    Route::group([
        "prefix" => "reports",
    ], function () {

        Route::get("/", "ReportsController@index");
        Route::post("create", "ReportsController@create");
        Route::post("update", "ReportsController@update");
        Route::post('delete', 'ReportsController@delete');

    });

    Route::group([
        "prefix" => "reservation",
    ], function () {
        Route::post("approvalstatus", "ReservationController@approvalstatus");
        Route::get("/", "ReservationController@index");
        Route::get("rejected", "ReservationController@rejected");
        Route::post("create", "ReservationController@create");
        Route::post("approval", "ReservationController@approval");
        Route::post("update", "ReservationController@update");
        Route::post('delete', 'ReservationController@delete');
        Route::post('restore', 'ReservationController@restore');

    });


    Route::group([
        "prefix" => "approval",
    ], function () {

        Route::get("/", "ApprovalController@index");
        Route::post("create", "ApprovalController@create");
        Route::post("update", "ApprovalController@update");
        Route::post('delete', 'ApprovalController@delete');

    });

    Route::group([
        "prefix" => "reviews",
    ], function () {

        Route::get("/", "ReviewsController@index");
        Route::post("create", "ReviewsController@create");
        Route::post("update", "ReviewsController@update");
        Route::post('delete', 'ReviewsController@delete');

    });

    Route::group([
        "prefix" => "delivery",
    ], function () {

        Route::get("/", "DeliveryController@index");
        Route::post("create", "DeliveryController@create");
        Route::post("update", "DeliveryController@update");
        Route::post('delete', 'DeliveryController@delete');

    });

    Route::group([
        "prefix" => "image",
    ], function () {

        Route::get("/", "ImageController@index");
        Route::post("create", "ImageController@create");
        Route::post("update", "ImageController@update");
        Route::post('delete', 'ImageController@delete');

    });

    Route::group([
        "prefix" => "follow",
    ], function () {

        Route::get("/", "followController@index");
        Route::post("create", "followController@create");
        Route::post("update", "followController@update");
        Route::post('delete', 'followController@delete');

    });


    Route::group([
        "prefix" => "saved_products",
    ], function () {

        Route::get("/", "saveController@index");
        Route::post("create", "saveController@create");
        Route::post("update", "saveController@update");
        Route::post('delete', 'saveController@delete');

    });

    Route::group([
        "prefix" => "specificsearch",
    ], function () {

        Route::get("/", "SpecificSearchController@index");
        Route::post("create", "SpecificSearchController@create");
        Route::post("update", "SpecificSearchController@update");
        Route::post('delete', 'SpecificSearchController@delete');

    });

    Route::group([
        "prefix" => "bonus",
    ], function () {

        Route::get("/", "bonusController@index");
        Route::post("create", "bonusController@create");
        Route::post("update", "bonusController@update");
        Route::post('delete', 'bonusController@delete');

    });

    Route::group([
        "prefix" => "notification",
    ], function () {

        Route::get("/", "notificationController@index");
        Route::get("user_notifications", "notificationController@user_notifications");
        Route::post("create", "notificationController@create");
        Route::post("update", "notificationController@update");
        Route::post('delete', 'notificationController@delete');

    });


    Route::group([
        "prefix" => "notificationtypes",
    ], function () {

        Route::get("/", "NotificationTypesController@index");
        Route::post("create", "NotificationTypesController@create");
        Route::post("update", "NotificationTypesController@update");
        Route::post('delete', 'NotificationTypesController@delete');

    });

    Route::group([
        "prefix" => "activities",
    ], function () {

        Route::get("/", "ActivitiesController@index");
        Route::post("create", "ActivitiesController@create");
        Route::post("update", "ActivitiesController@update");
        Route::post('delete', 'ActivitiesController@delete');

    });


    Route::group([
        "prefix" => "actiontypes",
    ], function () {

        Route::get("/", "ActionTypesController@index");
        Route::post("create", "ActionTypesController@create");
        Route::post("update", "ActionTypesController@update");
        Route::post('delete', 'ActionTypesController@delete');

    });

    Route::group([
        "prefix" => "closeddeal",
    ], function () {

        Route::post("/", "ClosedDealController@index");
        Route::post("create", "ClosedDealController@create");
        Route::post("update", "ClosedDealController@update");
        Route::post('delete', 'ClosedDealController@delete');

    });

    Route::group([
        "prefix" => "ratings",
    ], function () {

        Route::post("/", "RatingsController@index");
        Route::post("create", "RatingsController@create");
        Route::post("update", "RatingsController@update");
        Route::post('delete', 'RatingsController@delete');

    });


    Route::group([
        "prefix" => "userrating",
    ], function () {

        Route::post("/", "UserRatingController@index");
        Route::post("create", "UserRatingController@create");
        Route::post("update", "UserRatingController@update");
        Route::post('delete', 'UserRatingController@delete');

    });


    Route::group([
        "prefix" => "productrating",
    ], function () {

        Route::get("/", "ProductRatingController@index");
        Route::post("create", "ProductRatingController@create");
        Route::post("update", "ProductRatingController@update");
        Route::post('delete', 'ProductRatingController@delete');
        Route::post("getProductRating", "ProductRatingController@getProductRating"); Route::post("getProductRating", "ProductRatingController@getProductRating");

    });
    Route::group([
        "prefix" => "points",
    ], function () {

        Route::get("/", "PointsController@index");
        Route::get("getPoints", "PointsController@getPoints");
        Route::post("create", "PointsController@create");
        Route::post("update", "PointsController@update");
        Route::post('delete', 'PointsController@delete');

    });
    Route::group([
        "prefix" => "activity",
    ], function () {

        Route::post("read", "RawDataController@read");


    });
    Route::group([
        "prefix" => "contact",
    ], function () {

        Route::post("create", "ContactController@create");


    });

    Route::group([
        "prefix" => "request_contact",
    ], function () {    
        Route::post("/", "RequestContactController@getContactRequest");
        Route::post("create", "RequestContactController@create");


    });





});

