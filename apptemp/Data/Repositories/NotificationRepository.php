<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\NotificationModel;
use App\Data\Repositories\BaseRepository;
use App\Approval;
use App\Data\Models\Users;
use App\Data\Models\MessagesModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ReservationModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Models\ImageModel;
use App\User;
use App\Image;
use App\Reports;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class NotificationRepository extends BaseRepository
{

    protected $notification;
    protected $reservation;

    public function __construct(NotificationModel $notification,
    ReservationModel $reservation) 
    {
        $this->notification = $notification;
        $this->reservation = $reservation;
    }

    public function fetchnotification($data = [])
    {
        $meta_index = "notification";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "notification";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->notification);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
      
        if (!isset($data['notif_type_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "notif_type_id is not set.",
            ]);
        }

        if (!isset($data['action'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "action is not set.",
            ]);
        }

        if (!isset($data['sender_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "sender_id is not set.",
            ]);
        }

        if (!isset($data['receiver_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "receiver_id is not set.",
            ]);
        }

        if (!isset($data['description'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "description is not set.",
            ]);
        }

        $notification = $this->notification->init($this->notification->pullFillable($data));
        $notification->save($data);
        if (!$notification->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $notification->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully create notification.",
            "meta"        => [
                "status" => $notification,
            ]
        ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['reserved_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reserved_id is not set.",
            ]);
        }
        
        if (!isset($data['approved'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approved is not set.",
            ]);
        }

        if (!isset($data['rejected'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "rejected is not set.",
            ]);
        }
        $notification = $this->notification->find($data['id']);
        if($notification==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $notification->save($data);
        if (!$notification->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $notification->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a notification.",
            "meta"        => [
                "status" => $notification,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $notification = $this->notification->find($data['id']);
        if($notification==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$notification->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $notification->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a notification.",
            "meta"        => [
                "status" => $notification,
            ]
        ]);
            
        
    }

    public function user_notifications($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['single'] = true;
        $data['where'] = [
            [
                "target" => "receiver_id",
                "operator" => "=",
                "value" =>  auth()->user()->id ,
            ],
        ];

        $result = $this->fetchGeneric($data, $this->notification);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No notification found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully user notification",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);
    }

}
