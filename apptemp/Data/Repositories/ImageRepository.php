<?php

namespace App\Data\Repositories;

use App\Data\Models\ImageModel;
use App\Data\Repositories\BaseRepository;
use App\Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ImageRepository extends BaseRepository
{

    protected $image;

    public function __construct(ImageModel $image) 
    {
        $this->image = $image;
    }

    public function fetchimage($data = [])
    {
        $meta_index = "image";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "image";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->image);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Images are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        
    // if(isset($data['image']))
    //  {
       
    //     foreach($data['image'] as $image)
    //     {
            
    //         // $name=$image->getClientOriginalName();
    //         $imageName = time() . '.' . request()->image->getClientOriginalExtension();
    //         $file = $image->move('storage/images/', $imageName);  
    //         $url= asset($file);
    //         $data['imageName'] = $imageName;  
    //             $data['image_url'] = $data['images_url'];  
    //         $data['product_id'] =$data['product_id'];
             /***** */
            $image_url=null;
            if(isset($request['image'])){
                request()->validate([

                    'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',

                ]);
                $imageName = time().'.'.request()->image->getClientOriginalExtension();
                define('UPLOAD_DIR', 'storage/images/');
                $resize = Image::make($request['image'])->resize(400,400)->encode('jpg');   
                $file =  $resize->save(UPLOAD_DIR.$imageName,60);
                $url= asset(UPLOAD_DIR.$imageName);
                $image_url = $url;  
            
            }

            /***** */
                $image = $this->image->init($this->image->pullFillable($data));
                $image->save($data);    

            if (!$image->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $image->errors(),
                    ],
                ]);
            }

    return $this->setResponse([
        "code"       => 200,
        "title"      => "Successfully insert dynamic image.",
        "meta"      => $data,
        "parameters" => auth()->user()->id,
    ]);
            // return $this->setResponse([
            //     "code"       => 200,
            //     "title"      => "Successfully insert dynamic image.",
            //     "meta"      => $data,
            //     "parameters" => auth()->user()->id,
            // ]);
        
    }
    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['reserved_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reserved_id is not set.",
            ]);
        }
        
        if (!isset($data['approved'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approved is not set.",
            ]);
        }

        if (!isset($data['rejected'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "rejected is not set.",
            ]);
        }
        $image = $this->image->find($data['id']);
        if($image==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $image->save($data);
        if (!$image->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $image->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a image.",
            "meta"        => [
                "status" => $image,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $image = $this->image->find($data['id']);
        if($image==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$image->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $image->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a image.",
            "meta"        => [
                "status" => $image,
            ]
        ]);
            
        
    }


}
