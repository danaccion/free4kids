<?php

namespace App\Data\Repositories;

use App\Data\Models\ReservationModel;
use App\Data\Models\DelReservationModel;
use App\Data\Models\Product;
use App\Data\Repositories\BaseRepository;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use DateTime;
use DB;

class ReservationRepository extends BaseRepository
{

    protected $reservation;
    protected $del;

    public function __construct(
        ReservationModel $reservation,
        DelReservationModel $del,
        Product $product
        ) 
    {
        $this->reservation = $reservation;
        $this->del = $del;
        $this->product = $product;
    }

    public function fetchReservation($data = [])
    {
        $meta_index = "reservation";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "reservation";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->reservation);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation

        $product= $this->product->where([
            ['user_id', '=', auth()->user()->id]
    
        ])->get();
    
        // return $this->setResponse([
        //     'code'  => 500,
        //     'title' => $data['user_id'],
        //     'meta'=>$res,
            
        // ]);
        // $a = $this->reservation->find($data['product_id']);
        // $rese = $this->reservation::where('product_id', 5);//where('userReserveid', $data['userReserveid'])
        if($product->count() < 3)   
        return $this->setResponse([
                'code'  => 500,
                'title' => "Product is less than 3",
            ]);



        $prod = $this->product->find($data['product_id']);

          
            if (!isset($data['product_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "product_id is not set.",
                ]);         
            }
            
            // if (!isset($data['undo'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "undo is not set.",
            //     ]);
            // }

            // if (!isset($data['redo'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "redo is not set.",
            //     ]);
            // }
            // if (!isset($data['user_id'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "user_id is not set.",
            //     ]);
            // }
            // return $this->setResponse([
            //     "code"       => 200,
            //     "title"      => "Successfully reservation.",
            //     "parameters" => $prod,
            // ]);
            if($prod->user_id==auth()->user()->id){
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "Owner of product cannot reserve",
                ]);
              }
                $res= $this->reservation->where([

                    ['product_id', '=', $data['product_id']],
            
                    ['user_id', '=', $data['user_id']]
            
                ])->get();
            
                // return $this->setResponse([
                //     'code'  => 500,
                //     'title' => $data['user_id'],
                //     'meta'=>$res,
                    
                // ]);
                // $a = $this->reservation->find($data['product_id']);
                // $rese = $this->reservation::where('product_id', 5);//where('userReserveid', $data['userReserveid'])
                if($res->count() > 0)   
                return $this->setResponse([
                        'code'  => 500,
                        'title' => "Already reserve",
                    ]);
                // $users = DB::table('reservation')
                // ->Join('approval', 'reservation.id', '!=', 'approval.reserved_id')
                // ->where('reservation.created_at', '>',Carbon::now()->subDays(2));
                //     if(!$users->delete()){
                //         echo json_encode("All Clean");
                //         return;
                //     }
              
            

                
            $datetime = new DateTime();
            $datetime->modify('+2 day');
            $data['expiry']=$datetime->format('Y-m-d H:i:s');
            $data['owner_id']=$prod->user_id;
            $reservation = $this->reservation->init($this->reservation->pullFillable($data));
            $reservation->save($data);

            if (!$reservation->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $reservation->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "description" => "Action Completed",
                "title"      => "Successfuly Insert Reservation ",
                "parameters" => $reservation,
            ]);
        
    }

    public function approval($data = [])
    {
        if(!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }
        $reservation = $this->reservation->where("product_id",'=',$data['product_id'])->where("user_id",'=',$data['client_id'])->first();
        $reservation->approved=$data['approval'];
        $reservation->save();
        // if (!$reservation->save($data)) {
        //     return $this->setResponse([
        //         "code"        => 500,
        //         "title"       => "Data Validation Error.",
        //         "description" => "An error was detected on one of the inputted data.",
        //         "meta"        => [
        //             "errors" => $reservation->errors(),
        //         ],
        //     ]);
        // }
        
            return $this->setResponse([
                "code"        => 200,
                "title"       => "Approval Status",
                "description" => "Action Completed",
                "meta"        => [
                    "errors" => $reservation,
                ],
            ]);
      

    }

    public function update($data = [])
    {
        if(!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }
        if(!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }
        
        if (!isset($data['undo'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "undo is not set.",
            ]);
        }

        if (!isset($data['redo'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "redo is not set.",
            ]);
        }
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }
        $reservation = $this->reservation->find($data['id']);
        $reservation->save($data);
        if (!$reservation->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $reservation->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a reservation.",
            "meta"        => [
                "status" => $reservation,
            ]
        ]);
            
        
    }


    

    public function delete($data = [])
    {
        if (!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }

        $del = $this->reservation->find($data['product_id']);
        if($del==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$del->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $del->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a reservation.",
            "meta"        => [
                "status" => $del,
            ]
        ]);
            
        
    }


}
