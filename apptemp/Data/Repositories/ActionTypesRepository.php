<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\ActionTypesModel;
use App\Data\Repositories\BaseRepository;
use App\Approval;
use App\Data\Models\Users;
use App\Data\Models\MessagesModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ReservationModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Models\ImageModel;
use App\User;
use App\Image;
use App\Reports;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ActionTypesRepository extends BaseRepository
{

    protected $actiontypes;
    protected $reservation;

    public function __construct(ActionTypesModel $actiontypes,
    ReservationModel $reservation) 
    {
        $this->actiontypes = $actiontypes;
        $this->reservation = $reservation;
    }

    public function fetchActionTypes($data = [])
    {
        $meta_index = "actiontypes";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "actiontypes";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->actiontypes);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
      
       



        if (!isset($data['name'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "name is not set.",
            ]);
        }

        $actiontypes = $this->actiontypes->init($this->actiontypes->pullFillable($data));
        $actiontypes->save($data);
        if (!$actiontypes->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $actiontypes->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully create actiontypes.",
            "meta"        => [
                "status" => $actiontypes,
            ]
        ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['reserved_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reserved_id is not set.",
            ]);
        }
        
        if (!isset($data['approved'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approved is not set.",
            ]);
        }

        if (!isset($data['rejected'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "rejected is not set.",
            ]);
        }
        $actiontypes = $this->actiontypes->find($data['id']);
        if($actiontypes==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $actiontypes->save($data);
        if (!$actiontypes->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $actiontypes->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a actiontypes.",
            "meta"        => [
                "status" => $actiontypes,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $actiontypes = $this->actiontypes->find($data['id']);
        if($actiontypes==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$actiontypes->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $actiontypes->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a actiontypes.",
            "meta"        => [
                "status" => $actiontypes,
            ]
        ]);
            
        
    }


}
