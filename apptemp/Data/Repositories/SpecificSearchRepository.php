<?php

namespace App\Data\Repositories;

use App\Data\Models\SpecificSearchModel;
use App\Data\Repositories\BaseRepository;
use App\Reports;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class SpecificSearchRepository extends BaseRepository
{

    protected $search;

    public function __construct(SpecificSearchModel $search) 
    {
        $this->search = $search;
    }

    public function fetchspecific_search($data = [])
    {
        $meta_index = "search";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "search";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->search);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        
            if (!isset($data['feel'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "feel is not set.",
                ]);
            }
         
            if (!isset($data['user_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "user_id is not set.",
                ]);
            }

            if (!isset($data['size'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "size is not set.",
                ]);
            }

            if (!isset($data['able'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "able is not set.",
                ]);
            }

            if (!isset($data['shipping'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "shipping is not set.",
                ]);
            }

    
            $search = $this->search->init($this->search->pullFillable($data));
            $search->save($data);

            if (!$search->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $search->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully creaete search.",
                "parameters" => $search,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

      
        if (!isset($data['feel'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "feel is not set.",
            ]);
        }
     
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }

        if (!isset($data['size'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "size is not set.",
            ]);
        }

        if (!isset($data['able'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "able is not set.",
            ]);
        }

        if (!isset($data['shipping'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "shipping is not set.",
            ]);
        }


        $search = $this->search->find($data['id']);
        if($search==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $search->save($data);
        if (!$search->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $search->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a search.",
            "meta"        => [
                "status" => $search,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $search = $this->search->find($data['id']);
        if($search==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$search->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $search->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a search.",
            "meta"        => [
                "status" => $search,
            ]
        ]);
            
        
    }


}
