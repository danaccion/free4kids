<?php

namespace App\Data\Repositories;

use App\Data\Models\DeliveryModel;
use App\Data\Repositories\BaseRepository;
use App\Delivery;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class DeliveryRepository extends BaseRepository
{

    protected $delivery;

    public function __construct(DeliveryModel $delivery) 
    {
        $this->delivery = $delivery;
    }

    public function fetchDelivery($data = [])
    {
        $meta_index = "delivery";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "delivery";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->delivery);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        
          
            // if (!isset($data['user_id'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "user_id is not set.",
            //     ]);
            // }
            
            if (!isset($data['location_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "location_id is not set.",
                ]);
            }

            if (!isset($data['pay'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "pay is not set.",
                ]);
            }

            if (!isset($data['product_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "product_id is not set.",
                ]);
            }
    
            $delivery = $this->delivery->init($this->delivery->pullFillable($data));
            $delivery->save($data);

            if (!$delivery->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $delivery->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully delivery.",
                "parameters" => $delivery,
            ]);
        
    }

    public function update($data = [])  
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }
        
        if (!isset($data['location_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "location_id is not set.",
            ]);
        }

        if (!isset($data['pay'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "pay is not set.",
            ]);
        }
        $delivery = $this->delivery->find($data['id']);
        if($delivery==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $delivery->save($data);
        if (!$delivery->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $delivery->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a delivery.",
            "meta"        => [
                "status" => $delivery,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $delivery = $this->delivery->find($data['id']);
        if($delivery==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$delivery->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $delivery->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a delivery.",
            "meta"        => [
                "status" => $delivery,
            ]
        ]);
            
        
    }


}



// Place::leftJoin('rates', 'places.id', '=', 'rates.place_id')
//           ->selectRaw('places.*, SUM(rates.value) as sumOfRateValues')
//           ->groupBy('places.id')
//           ->orderBy('sumOfRateValues', 'DESC')
//           ->get();


//           $orders = DB::table('readings')
//                 ->orderBy('id', 'DESC')
//                 ->limit(1)
//                 ->get();


//                 mobile length  length 10 - temp
//                 database       length +2
//                                length 12 - mobile new length
//                                     i = length


                