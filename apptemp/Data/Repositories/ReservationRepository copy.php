<?php

namespace App\Data\Repositories;

use App\Data\Models\ReservationModel;
use App\Data\Models\DelReservationModel;
use App\Data\Models\ClosedDealModel;
use App\Data\Models\Product;
use App\Data\Repositories\BaseRepository;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use DateTime;
use DB;

class ReservationRepository extends BaseRepository
{

    protected $reservation;
    protected $del;
    protected $closeddeal;
    protected $seller_id;
    public function __construct(
        ClosedDealModel $closeddeal,
        ReservationModel $reservation,
        DelReservationModel $del,
        Product $product
        ) 
    {
        $this->reservation = $reservation;
        $this->del = $del;
        $this->closeddeal = $closeddeal;
        $this->product = $product;
    }

    public function fetchReservation($data = [])
    {
        $meta_index = "reservation";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "reservation";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->reservation);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function rejected($data = [])
    {
        $meta_index = "rejected";
        $parameters = [];
        $count = 0;

        $result = DB::table('reservation')->select('product.id as Product_id','product.name as Product_Name','users.name as User_name')
        ->Join('product', 'reservation.product_id', '=','product.id')
        ->Join('users', 'users.id', '=','reservation.user_id')
        // ->Join('approval', 'reservation.id', '=', 'approval.reserved_id')
        ->where('reservation.expiry', '!=',null)
        ->where('reservation.user_id', '=',auth()->user()->id)->get(); 
        
 

        // if (isset($data['id']) &&
        //     is_numeric($data['id'])) {

        //     $meta_index = "reservation";
        //     $data['single'] = true;
        //     $data['where'] = [
        //         [
        //             "target" => "id",
        //             "operator" => "=",
        //             "value" => $data['id'],
        //         ],
        //     ];

        //     $parameters['id'] = $data['id'];

        // }

        // $count_data = $data;

        // // $data['relations'][] = 'info';

        // $result = $this->fetchGeneric($data, $this->reservation);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved rejected",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }


    public function create($data = [])
    {
        // data validation
   
        $product= $this->product->where([
            ['user_id', '=', ($data['user_id']) ? :auth()->user()->id]
    
        ])->get();


        $product_id= $this->product->where([
            ['id', '=',  $data['product_id']]
    
        ])->get();
        

        $close = $this->closeddeal->where([
            ['product_id', '=',  $data['product_id']]
    
        ])->get();


        /* ERROR HANDLING */

        if($product_id->count() == 0)  { 
            return $this->setResponse([
                    'code'  => 500,
                    'title' => "This product does not exist",
                ]);
        }
  
        if($close->count() != 0)  { 
        return $this->setResponse([
                'code'  => 500,
                'title' => "This product is already closed",
            ]);
        }
            
        if($product->count() < 3){  
        return $this->setResponse([
                'code'  => 500,
                'title' => "Product is less than 3",
            ]);
        }
        $prod = $this->product->find($data['product_id']);
          
         if (!isset($data['product_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "product_id is not set.",
                ]);         
            }
        
        if($prod->user_id== $data['user_id']){
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "Owner of product cannot reserve",
                ]);
              }

              /* CONDITIONS */
                $res= $this->reservation->where([

                    ['product_id', '=', $data['product_id']],
            
                    ['user_id', '=', $data['user_id']]
            
                ])->get();
            
                $pending= $this->reservation->where([

                    ['user_id', '=',$data['user_id']],

                    ['owner_id', '=',$prod->user_id]

                    ])->get();



         /* HANDLING */
         if($res->count() > 0)   
         return $this->setResponse([
                 'code'  => 404,
                 'title' => "Already reserve",
             ]);
             
                if($pending->count() > 0)   
                return $this->setResponse([
                        'code'  => 404,
                        'title' => "Cant reserve in the same shop ,closed deal before choosing another product",
                    ]);

        
         
            $datetime = new DateTime();
            $datetime->modify('+2 day');
            $data['expiry']=$datetime->format('Y-m-d H:i:s');
            $data['owner_id']=$prod->user_id;
            $reservation = $this->reservation->init($this->reservation->pullFillable($data));
            $reservation->save($data);

            if (!$reservation->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $reservation->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "description" => "Action Completed",
                "title"      => "Successfuly Insert Reservation ",
                "parameters" => $reservation,
            ]);
        
    }

    public function approval($data = [])
    {
        if(!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }
        $reservation = $this->reservation->where("product_id",'=',$data['product_id'])->where("user_id",'=',$data['client_id'])->first();
        $reservation->approved=$data['approval'];
        $reservation->save();
        // if (!$reservation->save($data)) {
        //     return $this->setResponse([
        //         "code"        => 500,
        //         "title"       => "Data Validation Error.",
        //         "description" => "An error was detected on one of the inputted data.",
        //         "meta"        => [
        //             "errors" => $reservation->errors(),
        //         ],
        //     ]);
        // }
        
            return $this->setResponse([
                "code"        => 200,
                "title"       => "Approval Status",
                "description" => "Action Completed",
                "meta"        => [
                    "errors" => $reservation,
                ],
            ]);
      

    }

    public function update($data = [])
    {
        if(!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }
        if(!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }
        
        if (!isset($data['undo'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "undo is not set.",
            ]);
        }

        if (!isset($data['redo'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "redo is not set.",
            ]);
        }
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }
        $reservation = $this->reservation->find($data['id']);
        $reservation->save($data);
        if (!$reservation->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $reservation->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a reservation.",
            "meta"        => [
                "status" => $reservation,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $reservation = $this->reservation->find($data['id']);
        if($reservation==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$reservation->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $reservation->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a reservation.",
            "meta"        => [
                "status" => $reservation,
            ]
        ]);
            
        
    }


    public function restore($data = [])
    {
        if (!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }

        $del = 
        $this->del ::withTrashed()
        ->where('product_id', $data['product_id'])
        ->where('deleted_at', '!=' , null)
        ->restore();
        if($del==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        
        if (!$del) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $del->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully restore.",
        ]);
           
        


        
    }


    public function approvalstatus($data = [])
    {
        // data validation
        
          
            // if (!isset($data['reserved_id'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "reserved_id is not set.",
            //     ]);
            // }
            // if (!isset($data['lack_of_approved'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "reserved_id is not set.",
            //     ]);
            // }
            // if (!isset($data['rejected'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "rejected is not set.",
            //     ]);
            // }
            // if (!isset($data['approved'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "approved is not set.",
            //     ]);
            // }

       

        //     $approval = $this->approval->init($this->approval->pullFillable($data));
        //     $approval->save($data);


        // $meta_index = "users";

       
        // $userObj = Users::all();
        // $prodObj = ReservationModel::all()->where('id',$data['reserved_id']);
        // $result = [];
        // foreach ($userObj as $key => $value) {
        //             $user_id = $value->id;
                   
                   
        //             foreach ($prodObj as $key => $value2) {
        //               if($value2->user_id == $user_id)
        //               {
        //                 array_push($result, $value);
        //                 $prod_id = $value2->id;
        //                // array_push($result, "reservation");
        //               //  array_push($result, $value2);
        //               }
        //             }
        //     }
            
            
        //     $a["Users"] = $result;
        //     $a["Request"][] = $approval;
        
        $reservation = $this->reservation->find($data['id']);
        
        $reservation->save($data);
        if (!$reservation->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $reservation->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a reservation.",
            "meta"        => [
                "status" => $reservation,
            ]
        ]);
        
    }
}

