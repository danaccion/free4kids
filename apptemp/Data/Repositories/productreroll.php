<?php

namespace App\Data\Repositories;

use App\Data\Models\Product;
use App\Data\Models\DeliveryModel;
use App\Data\Models\ImageModel;
use App\Data\Repositories\BaseRepository;
use App\Delivery;
use App\Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
require '../vendor/autoload.php';
use Intervention\Image\ImageManagerStatic as Images;

class ProductRepository extends BaseRepository
{

    protected $product;
    protected $image;
    protected $delivery;


    protected $hidden = [
        'deleted_at','updated_at','created_at'
        
    ];

    public function __construct(Product $product, DeliveryModel $delivery, ImageModel $image) 
    {
        $this->product = $product;
        $this->delivery = $delivery;
        $this->image = $image;
    }

    public function fetchProduct($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["location"];
        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "product";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "user_id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->product);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function productReviews($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["reviews"];
        if (isset($data['product_id']) &&
            is_numeric($data['product_id'])) {

            $meta_index = "product";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['product_id'],
                ],
                [
                    "target" => "user_id",
                    "operator" => "=",
                    "value" => auth()->user()->id,
                ]
            ];
            $parameters['product_id'] = $data['product_id'];
        }else{
            $data['where'] = [
                [
                    "target" => "user_id",
                    "operator" => "=",
                    "value" => auth()->user()->id,
                ]
            ];
        }
        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->product);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }


    public function maxProducts($data = [])
    {
        $meta_index = "totalproduct";
        $parameters = [];
        $count = 0;
        $result = $this->product->max('id');


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }


//     public function create($data = [])
//     {

//         $imageObj = ImageModel::where('user_id',$data['user_id']) 
//         ->orderBy('id', 'desc')
//         ->take(1)
//         ->get('id');
        

//         foreach ($imageObj as $key => $value) {
//             $image_id = $value->id;
//         }
//         $data['image_id'] = $image_id;
//             if (!isset($data['image_id'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "image id is not set.",
//                 ]);
//             }
//             if (!isset($data['user_id'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "user id is not set.",
//                 ]);
//             }

          
        
//             if (!isset($data['name'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "name is not set.",
//                 ]);
//             }

//             if (!isset($data['pay'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "pay is not set.",
//                 ]);
//             }

//             if (!isset($data['product_lat'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "product_latis not set.",
//                 ]);
//             }

//             if (!isset($data['product_lng'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "product_lng is not set.",
//                 ]);
//             }

//             if (!isset($data['location'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "location is not set.",
//                 ]);
//             }

//             if (!isset($data['size'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "size is not set.",
//                 ]);
//             }


//             if (!isset($data['sex'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "sex is not set.",
//                 ]);
//             }

//             if (!isset($data['brand'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "brand is not set.",
//                 ]);
//             }

//             if (!isset($data['stand'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "stand is not set.",
//                 ]);
//             }
            
//             if (!isset($data['description'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "description is not set.",
//                 ]);
//             }
//                 if (!isset($data['category_id'])) {
//                     return $this->setResponse([
//                         'code'  => 500,
//                         'title' => "category id is not set.",
//                     ]);
//                 }

		
//         /*     return $this->setResponse([
//                      'code'  => 500,
//                      'title' =>   $data['image_url'],
//                  ]);*/
//             $product = $this->product->init($this->product->pullFillable($data));
//             $product->save($data);


        



//             $prodObj = Product::all();

//             /***********************************/
    
//             $delivery = $this->delivery->init($this->delivery->pullFillable($data));
//             $delivery->save($data);

//             $result = [];
//             $product_id = 0;
//             array_push($result, "product");
//             array_push($result, $product);

//             array_push($result, "delivery");
//             array_push($result, $delivery);

//         //    array_push($result, "image");
//           //  array_push($result, $image);


            
//             $prodObj = $product::where('user_id',$data['user_id']) 
//             ->orderBy('id', 'desc')
//             ->take(1)
//             ->get('id');


//             foreach ($prodObj as $key => $value) {
//                 $product_id = $value->id;
//             }
		
// 		$data['product_id'] =  $product_id;
// //return $this->setResponse([
//   //                   'code'  => 500,
//     //                 'title' => $product_id,
//       //           ]);

//             //temp location id
//             // if (!isset($data['location_id'])) {
//             //     return $this->setResponse([
//             //         'code'  => 500,
//             //         'title' => "location_id is not set.",
//             //     ]);
//             // }

		
// 			if(isset($data['image'])){
// 			$image_url=null;
//        /*     request()->validate([

//                 'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',
    
//             ]);*/
//             $imageName = time().'.'.request()->image->getClientOriginalExtension();
//             define('UPLOAD_DIR', 'storage/images/');
//             $resize = Images::make($data['image'])->resize(400,400)->encode('jpg');   
//             $file =  $resize->save(UPLOAD_DIR.$imageName,60);
//             $url= asset(UPLOAD_DIR.$imageName);
//            $data['image_url'] = $url;
//                 $image = $this->image->init($this->image->pullFillable($data));
//                $image->save($data);
//         }
//             if (!isset($data['pay'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "pay is not set.",
//                 ]);
//             }
//             $data['product_id'] = $product_id;
//             if (!isset($data['product_id'])) {
//                 return $this->setResponse([
//                     'code'  => 500,
//                     'title' => "product_id is not set.",
//                 ]);
//             }


//             if (!$delivery->save($data)) {
//                 return $this->setResponse([
//                     "code"        => 500,
//                     "title"       => "Data Validation Error.",
//                     "description" => "An error was detected on one of the inputted data.",
//                     "meta"        => [
//                         "errors" => $delivery->errors(),
//                     ],
//                 ]);
//             }

//             if (!$product->save($data)) {
//                 return $this->setResponse([
//                     "code"        => 500,
//                     "title"       => "Data Validation Error.",
//                     "description" => "An error was detected on one of the inputted data.",
//                     "meta"        => [
//                         "errors" => $product->errors(),
//                     ],
//                 ]);
//             }
// 			$image_url = $data['image_url'];
//             return $this->setResponse([
//                 "code"       => 200,
//                 "title"      => "Successfully create product, delivery, image.",
//                 "parameters" =>$result,
// 				    'image_url' => $image_url,
//                  "meta" => [
//                 'image_url' => $image_url,
//             ],
//             ]);
  
        
//     }

    /*
    'id','size','category_id','name', 'user_id','id','location',
    'product_lat','product_lng','image_id','age','sex','brand',
    'stand','description','expiry_date',
        'delivery_type'
    */


    public function create($data = [])
    {
     

			if(isset($data['image'])){
			$image_url=null;
       /*     request()->validate([

                'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',
    
            ]);*/
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            define('UPLOAD_DIR', 'storage/images/');
            $resize = Images::make($data['image'])->resize(400,400)->encode('jpg');   
            $file =  $resize->save(UPLOAD_DIR.$imageName,60);
            $url= asset(UPLOAD_DIR.$imageName);
           $data['image_url'] = $url;
                $image = $this->image->init($this->image->pullFillable($data));
               $image->save($data);
        }
        $productwithobj = Product::where('user_id',$data['user_id']) 
        ->orderBy('id', 'desc')
        ->take(1)
        ->get('id');
        

        foreach ($productwithobj as $key => $value) {
            $product_id = $value->id;
        }
        $data['product_id'] = $product_id;
        // return $this->setResponse([
        //     "code"       => 200,
        //     "title"      =>   $data['product_id'] ,
        //     "meta"        => [
        //         "status" => $product,
        //     ]
        // ]);

        $product = $this->product->init($this->product->pullFillable($data));
        $product->save($data);


        $image = $this->image->init($this->image->pullFillable($data));
        $image->save($data);

            $delivery = $this->delivery->init($this->delivery->pullFillable($data));
            $delivery->save($data);

                        if (!$delivery->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $delivery->errors(),
                    ],
                ]);
            }
        
        if (!$product->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $product->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully create product.",
            "meta"        => [
                "status" => $product,
            ]
        ]);
        
    }


    public function update($data = [])
    {

    
        // if (!isset($data['id'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "id is not set.",
        //     ]);
        // }

        // if (!isset($data['user_id'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "user id is not set.",
        //     ]);
        // }

        // if (!isset($data['category_id'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "category id is not set.",
        //     ]);
        // }

        
        //     if (!isset($data['name'])) {
        //         return $this->setResponse([
        //             'code'  => 500,
        //             'title' => "name is not set.",
        //         ]);
        //     }

        //     if (!isset($data['image'])) {
        //         return $this->setResponse([
        //             'code'  => 500,
        //             'title' => "image is not set.",
        //         ]);
        //     }else{
        //         $data['image_url']=null;      
        //             request()->validate([
        //                 'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',
            
        //             ]);
        //             $imageName = time().'.'.request()->image->getClientOriginalExtension();
        //             define('UPLOAD_DIR', 'storage/images/');
        //             $file =  request()->image->move(UPLOAD_DIR,$imageName);
        //             $url= asset($file);
        //             $data['image_url'] = $url;        
        //     }
            
        $product = $this->product->find($data['id']);
        
        if($product==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }
    
        $product->save($data);
        if (!$product->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $product->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a product.",
            "meta"        => [
                "status" => $product,
            ]
        ]);
            
        
    }
    
    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $product = $this->product->find($data['id']);
        if($product==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$product->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $product->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a product.",
            "meta"        => [
                "status" => $product,
            ]
        ]);
            
        
    }

  


}
