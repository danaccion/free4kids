<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\ClosedDealModel;
use App\Data\Repositories\BaseRepository;
use App\Approval;
use App\Data\Models\Users;
use App\Data\Models\MessagesModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ReservationModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Models\ImageModel;
use App\User;
use App\Image;
use App\Reports;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ClosedDealRepository extends BaseRepository
{

    protected $closeddeal;
    protected $reservation;

    public function __construct(ClosedDealModel $closeddeal,
    ReservationModel $reservation) 
    {
        $this->closeddeal = $closeddeal;
        $this->reservation = $reservation;
    }

    public function fetchClosedDeal($data = [])
    {
        $meta_index = "closeddeal";
        $parameters = [];
        $count = 0;

        $data['relations'] = ["Seller"];
        $data['wherehas'][] = [
            'relation' => 'Buyer',
            'target' => [
                [
                    'column' => "buyer_id",
                    "operator" => "=",
                    "value" => auth()->user()->id,
                ],
            ],
        ];


        $data['wherehas'][] = [
            'relation' => 'Seller',
            'target' => [
                [
                    'column' => "seller_id",
                    "operator" => "=",
                    "value" => $data['seller_id'],
                ],
            ],
        ];
        // if (isset($data['id']) &&
        //     is_numeric($data['id'])) {

        //     $meta_index = "closeddeal";
        //     $data['single'] = true;
        //     $data['where'] = [
        //         [
        //             "target" => "id",
        //             "operator" => "=",
        //             "value" => $data['id'],
        //         ],
        //     ];

        //     $parameters['id'] = $data['id'];

        // }

        // $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->closeddeal);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved closed deal",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }


    public function create($data = [])
    {
      
        if (!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }

        if (!isset($data['buyer_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "buyer_id is not set.",
            ]);
        }


        if (!isset($data['seller_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "seller_id is not set.",
            ]);
        }

        $closeddeal = $this->closeddeal->init($this->closeddeal->pullFillable($data));
        $closeddeal->save($data);
        if (!$closeddeal->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $closeddeal->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully create closeddeal.",
            "meta"        => [
                "status" => $closeddeal,
            ]
        ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['reserved_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reserved_id is not set.",
            ]);
        }
        
        if (!isset($data['approved'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approved is not set.",
            ]);
        }

        if (!isset($data['rejected'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "rejected is not set.",
            ]);
        }
        $closeddeal = $this->closeddeal->find($data['id']);
        if($closeddeal==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $closeddeal->save($data);
        if (!$closeddeal->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $closeddeal->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a closeddeal.",
            "meta"        => [
                "status" => $closeddeal,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $closeddeal = $this->closeddeal->find($data['id']);
        if($closeddeal==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$closeddeal->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $closeddeal->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a closeddeal.",
            "meta"        => [
                "status" => $closeddeal,
            ]
        ]);
            
        
    }


}
