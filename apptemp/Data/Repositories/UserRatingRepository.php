<?php

namespace App\Data\Repositories;
use DB;

use App\Data\Models\UserRatingModel;
use App\Data\Models\ApprovalModel;
use App\Data\Models\followModel;
use App\Data\Models\MessagesModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ProductUserid;
use App\Data\Models\ReservationModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Models\ReviewsModel;
use App\Data\Models\saveModel;
use App\Data\Repositories\BaseRepository;
use App\Data\Models\ImageModel;
use App\User;
use App\Products;
use App\Image;
use App\Reports;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserRatingRepository extends BaseRepository
{

    protected $userrating;
    protected $image;
    protected $reservation;
    protected $products;
    protected $category;
    protected $saveModel;
    protected $puid;
    protected $follow;
    protected $messages;
    protected $reviews;
    public function __construct(
        UserRatingModel $userrating , Location $location, 
        ReportsModel $reports, 
        Product $products,
        ProductUserid $puid,
        ImageModel $image,
        ReservationModel $reservation,
        saveModel $saveModel,
        ProductCategory $category,
        followModel $follow,
        MessagesModel $messages,
        ReviewsModel $reviews
    ) {
        $this->userrating = $userrating;
        $this->location = $location;
        $this->reports = $reports;
        $this->products = $products;
        $this->image = $image;
        $this->reservation = $reservation;
        $this->saveModel = $saveModel;
        $this->category = $category;
        $this->puid = $puid;
        $this->follow = $follow;
		$this->messages = $messages;
    }

    public function fetchUser($data = [])
    {
        $meta_index = "UserRatingModel";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "UserRatingModel";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->UserRatingModel);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->UserRatingModel->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function location($data = [])
    {
        $meta_index = "UserRatingModel";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["location"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {

            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
            ];

            $parameters['user_id'] = $data['user_id'];

        }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->UserRatingModel);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->UserRatingModel->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function reports($data = [])
    {
        $meta_index = "UserRatingModel";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["reports"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {

            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
            ];

            $parameters['user_id'] = $data['user_id'];

        }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->UserRatingModel);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->UserRatingModel->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }
    public function products($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['relations'] = ["image","UserRatingModel","delivery","location"];
      
       
        if (isset($data['category_id'])) {
            $data['wherehas'][] = [
                'relation' => 'productstocategory',
                'target' => [
                    [
                        'column' => 'category_id',
                        'value' => $data['category_id'],
                    ],
                ],
            ];
        }
        if (isset($data['user_id'])) {
            $data['wherehas'][] = [
                'relation' => 'products',
                'target' => [
                    [
                        'column' => 'user_id',
                        'value' => $data['user_id'],
                    ],
                ],
            ];
        }

        $result = $this->fetchGeneric($data, $this->products);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->UserRatingModel->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
            ],
            "parameters" => $parameters,
        ]);
    } 

    public function searchProducts($data = [])
    {
        if (!isset($data['query'])) {
            return $this->setResponse([
                "code" => 500,
                "title" => "Query is not set",
                "parameters" => $data,
            ]);
        }

        $meta_index = "product";
        $parameters = [
            "query" => $data['query'],
        ];
        $result = $this->products;
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['relations'] = ["image","UserRatingModel","delivery","location"];
      
       
        if (isset($data['category_id'])) {
            $data['wherehas'][] = [
                'relation' => 'productstocategory',
                'target' => [
                    [
                        'column' => 'category_id',
                        'value' => $data['category_id'],
                    ],
                ],
            ];
        }
        if (isset($data['user_id'])) {
            $data['wherehas'][] = [
                'relation' => 'products',
                'target' => [
                    [
                        'column' => 'user_id',
                        'value' => $data['user_id'],
                    ],
                ],
            ];
        }

        if (isset($data['target'])) {
            foreach ((array) $data['target'] as $index => $column) {
                if (str_contains($column, 'name')) {
                    $data['target'][] = 'name';
                    unset($data['target'][$index]);
                }
            }

        }

        $result = $this->genericSearch($data, $result)->get()->all();

        if ($result == null) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No products are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count_data['search'] = true;
        // $result = $this->fetchGeneric($data, $this->products);

        // if (!$result) {
        //     return $this->setResponse([
        //         'code' => 404,
        //         'title' => "No agents are found",
        //         "meta" => [
        //             $meta_index => $result,
        //         ],
        //         "parameters" => $parameters,
        //     ]);
        // }

        // $count = $this->countData($count_data, refresh_model($this->UserRatingModel->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
            ],
            "parameters" => $parameters,
        ]);
    } 

    public function saved_products($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['relations'] = ["products","UserRatingModel"];
        $data['where'] = [
            [
                "target" => "user_id",
                "operator" => "=",
                "value" => auth()->user()->id,
            ],
        ];
		
        $result = $this->fetchGeneric($data, $this->saveModel);
		// $reservation = $this->image->where("product_id",'=',$this->products->product_id)->first();
   
        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->UserRatingModel->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    } 

    public function set_category_id($data = [])
    {
        $meta_index = "category";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['relations'] = ["products"];
        if (isset($data['category_id'])) {
            $data['wherehas'][] = [
                'relation' => 'product_category',
                'target' => [
                    [
                        'column' => 'id',
                        'value' => $data['category_id'],
                    ],
                ],
            ];
        }
        if (isset($data['user_id'])) {
            $data['wherehas'][] = [
                'relation' => 'products',
                'target' => [
                    [
                        'column' => 'user_id',
                        'value' => $data['user_id'],
                    ],
                ],
            ];
        }
        // $meta_index = "UserRatingModel";
        // $parameters = [];
        // $count = 0;
        // $data['relations'] = ["products"];
        // $data['sort'] = 'created_at';
        // $data['order'] = 'desc';
        // if (isset($data['user_id']) &&
        //     is_numeric($data['user_id'])) { 
        //     $data['single'] = true;
        //     $data['where'] = [
        //         [
        //             "target" => "id",
        //             "operator" => "=",
        //             "value" => $data['user_id'],
        //         ],
        //     ];
        //     $parameters['user_id'] = $data['user_id'];
        // }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->category);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->UserRatingModel->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    } 

    public function product_category($data = [])
    {
        // $meta_index = "UserRatingModel";
        // $pcatObj = ProductCategory::all();
        // $prodObj = Product::all();
        // $userObj = UserRatingModel::all()->where('id',$data['user_id']);
        // $result = [];
        // foreach ($userObj as $key => $value) {
        //             $user_id = $value->id;
        //             array_push($result, "UserRatingModel");
        //             array_push($result, $value);
        //             foreach ($prodObj as $key => $value2) {
        //               if($value2->user_id == $user_id)
        //               {
        //                 $prod_id = $value2->id;
        //                 array_push($result, "product");
        //                 array_push($result, $value2);
        //                 foreach ($pcatObj as $key => $value3) {
        //                     if($value3->id== $prod_id)
        //                     {
        //                       array_push($result, "product category");
        //                       array_push($result, $value3);
        //                     }
        //                   }
        //               }
        //             }
        //     }
         $meta_index = "category";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["products"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) { 
            $data['single'] = false;
            $data['wherehas'][] = [
                'relation' => 'reservation',
                'target' => [
                    [
                        'column' => 'reservation.product_id',
                        'operator' => '!=',
                        'value' =>'null',
                    ]
                             ]
                    ];
            $parameters['user_id'] = $data['user_id'];
        }

    
        $result = $this->fetchGeneric($data, $this->category);



        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No UserRatingModel are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved UserRatingModel->product->category",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    } 

    public function all_product_category($data = [])
    {

        $meta_index = "category";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["products"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) { 
            $data['single'] = false;
            $data['wherehas'][] = [
                'relation' => 'reservation',
                'target' => [
                    [
                        'column' => 'reservation.product_id',
                        'operator' => '!=',
                        'value' =>'null',
                    ]
                ]
                    ];
            $parameters['user_id'] = $data['user_id'];
        }

    
        $result = $this->fetchGeneric($data, $this->category);

        // $meta_index = "UserRatingModel";
        // $presObj = ReservationModel::all();
        // $prodObj = Product::all();
        // $userObj = UserRatingModel::all()->where('id',$data['user_id']);
        // $result = [];
        // foreach ($userObj as $key => $value) {
        //             $user_id = $value->id;
        //             array_push($result, "UserRatingModel");
        //             array_push($result, $value);
        //             foreach ($prodObj as $key => $value2) {
        //               if($value2->user_id == $user_id)
        //               {
        //                 $prod_id = $value2->id;
        //                 array_push($result, "product");
        //                 array_push($result, $value2);
        //                 foreach ($presObj as $key => $value3) {
        //                     if($value3->product_id == $prod_id)
        //                     {
        //                       array_push($result, "product reservation");
        //                       array_push($result, $value3);
        //                     }
        //                   }
        //               }
        //             }
        //     }


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No UserRatingModel are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved product - > reservation",
            "description" => "parameter either with user_id | post method",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    } 



    public function reviews($data = [])
    {
        $meta_index = "UserRatingModel";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["reviews"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {

            $meta_index = "UserRatingModel";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
            ];

            $parameters['user_id'] = $data['user_id'];

        }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->UserRatingModel);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->UserRatingModel->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved profile reviews",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }


    public function reservation($data = [])
    {
    //:params user_id,product_id 
        $meta_index = "reservation";
        $parameters = [];
        $count = 0;
        $data['where'] = [
            [
                "target" => "user_id",
                "operator" => "=",
                "value" => auth()->user()->id,
            ],
            [
                "target" => "owner_id",
                "operator" => "!=",
                "value" => null,
            ],
        ];
        $data['relations'] = ["products","UserRatingModel"];
        // if (isset($data['user_id']) &&
        //     is_numeric($data['user_id']) &&
        //     isset($data['product_id']) &&
        //     is_numeric($data['product_id'])
        //     ) { 
        //     $data['single'] = false;
        //     $data['wherehas'][] = [
        //         'relation' => 'product',
        //         'target' => [
        //             [
        //                 'column' => 'product.user_id',
        //                 'operator' => '=',
        //                 'value' =>$data['user_id'],
        //             ]
        //             ],
        //             'target' => [
        //                 [
        //                     'column' => 'product.id',
        //                     'operator' => '=',
        //                     'value' =>$data['product_id'],
        //                 ]
        //                 ],

        //             ];
        //     $parameters['user_id'] = $data['user_id'];
        // }

        // if (isset($data['user_id']) &&
        //     is_numeric($data['user_id'])) { 
        //     $data['single'] = false;
        //     $data['wherehas'][] = [
        //         'relation' => 'product',
        //         'target' => [
        //             [
        //                 'column' => 'product.user_id',
        //                 'operator' => '=',
        //                 'value' =>$data['user_id'],
        //             ]
        //             ],
        //             ];
        //     $parameters['user_id'] = $data['user_id'];
        // }
        
    
        $result = $this->fetchGeneric($data, $this->reservation);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No reservation found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved product - > reservation",
            "description" => "parameter either with user_id | post method",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    } 

    // public function approval($data = [])
    // {
    //     // $meta_index = "UserRatingModel";
    //     // $presObj = ReservationModel::all();
    //     // $approObj = ApprovalModel::all();
    //     // $prodObj = Product::all();
    //     // $userObj = UserRatingModel::all()->where('id',$data['user_id']);
    //     // $result = [];
    //     // foreach ($userObj as $key => $value) {
    //     //             $user_id = $value->id;
    //     //             array_push($result, "UserRatingModel");
    //     //             array_push($result, $value);
    //     //             foreach ($prodObj as $key => $value2) {
    //     //               if($value2->user_id == $user_id)
    //     //               {
    //     //                 $prod_id = $value2->id;
    //     //                 array_push($result, "product");
    //     //                 array_push($result, $value2);
    //     //                 foreach ($presObj as $key => $value3) {
    //     //                     if($value3->product_id == $prod_id)
    //     //                     {
    //     //                      $reserved_id = $value3->id;    
    //     //                       array_push($result, "product reservation");
    //     //                       array_push($result, $value3);

    //     //                       foreach ($approObj as $key => $value4) {
    //     //                         if($value4->reserved_id == $reserved_id)
    //     //                         {
    //     //                           array_push($result, "approval");
    //     //                           array_push($result, $value4);
    //     //                         }
    //     //                       }
    //     //                     }
    //     //                   }
    //     //               }
    //     //             }
    //     //     }

    //     $meta_index = "reservation";
    //     $parameters = [];
    //     $count = 0;
    //     $data['relations'] = ["approval"];
    //     if (isset($data['user_id']) &&
    //         is_numeric($data['user_id'])) { 
    //         $data['single'] = false;
    //         $data['wherehas'][] = [
    //             'relation' => 'approval',
    //             'target' => [
    //                 [
    //                     'column' => 'approval.reserved_id',
    //                     'operator' => '!=',
    //                     'value' =>'null',
    //                 ]
    //             ]
    //                 ];
    //         $parameters['user_id'] = $data['user_id'];
    //     }

    
    //     $result = $this->fetchGeneric($data, $this->reservation);
    //     if (!$result) {
    //         return $this->setResponse([
    //             'code' => 404,
    //             'title' => "No UserRatingModel are found",
    //             "meta" => [
    //                 $meta_index => $result,
    //             ],
    //         ]);
    //     }
    //     return $this->setResponse([
    //         "code" => 200,
    //         "title" => "Successfully retrieved reservation->approval",
    //         "description" => "UserInfo",
    //         "meta" => [
    //             $meta_index => $result,
    //         ],

    //     ]);

    // } 

    public function approval($data = [])
    {
    //:params user_id,product_id 
        $meta_index = "approval";
        $parameters = [];
        $count = 0;
      
       
        if(!isset($data['user_id']))
        {
            $data['relations'] = ["approval_product","approval_user"];
            
            $data['where'] = [
                [
                    "target" => "owner_id",
                    "operator" => "=",
                    "value" => auth()->user()->id,
                ],
            ];
            
        }elseif(isset($data['user_id']))
        {
            $data['relations'] = ["approval_product","approval_user"];
            
            $data['where'] = [
                [
                    "target" => "owner_id",
                    "operator" => "=",
                    "value" => auth()->user()->id,
                ],
            ];
        }

     
         $result = $this->fetchGeneric($data, $this->reservation);
        

        //  return $this->setResponse([
        //     'code' => 404,
        //     'title' => auth()->user()->id,
            
        // ]);

        // if (isset($data['user_id']) &&
        //     is_numeric($data['user_id']) &&
        //     isset($data['product_id']) &&
        //     is_numeric($data['product_id'])
        //     ) { 
        //     $data['single'] = false;
        //     $data['wherehas'][] = [
        //         'relation' => 'approval',
        //         'target' => [
        //             [
        //                 'column' => 'approval.reserved_id',
        //                 'operator' => '=',
        //                 'value' =>'approval.id',
        //             ]
        //             ],
        //             'target' => [
        //                 [
        //                     'column' => 'approval.approved',
        //                     'operator' => '=',
        //                     'value' =>'1',
        //                 ]
        //                 ],

        //             ];
        //     $parameters['user_id'] = $data['user_id'];
        // }

        // if (isset($data['user_id']) &&
        //     is_numeric($data['user_id'])) { 
        //     $data['single'] = false;
        //     $data['wherehas'][] = [
        //         'relation' => 'product',
        //         'target' => [
        //             [
        //                 'column' => 'product.user_id',
        //                 'operator' => '=',
        //                 'value' =>$data['user_id'],
        //             ]
        //             ],
        //             ];
        //     $parameters['user_id'] = $data['user_id'];
        // }

    
        // $result = $this->fetchGeneric($data, $this->puid);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No UserRatingModel are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved approval",
            "description" => "parameter either with post method",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    } 
    //follower ikaw is user_id
    public function followers($data = [])
    {
    //:params user_id,product_id 
        $meta_index = "follow";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["Followersid"];
        if(!isset($data['user_id']))
        {
            
            $data['wherehas'][] = [
                'relation' => 'follow',
                'target' => [
                    [
                        'column' => "user_id",
                        "operator" => "=",
                        "value" => auth()->user()->id,
                    ],
                ],
              

                
            ];


        }elseif(isset($data['user_id']))
        {
            
            $data['wherehas'][] = [
                'relation' => 'follows',
                'target' => [
                    [
                        'column' => "user_id",
                        "operator" => "=",
                        "value" => $data['user_id'],
                    ],
                ],
            ];
        }

     
         $result = $this->fetchGeneric($data, $this->follow);
        


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Followers are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved Followers",
            "description" => "parameter either with post method",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    } 
    //you follow ikaw si follwer
    public function you_follow($data = [])
    {
    //:params user_id,product_id 
        $meta_index = "follow";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["UserRatingModelid"];
       if(isset($data['user_id']))
        {
            
            $data['wherehas'][] = [
                'relation' => 'follows',
                'target' => [
                    [
                        'column' => "follower",
                        "operator" => "=",
                        "value" => $data['user_id'],
                    ],
                ],
            ];
            }else{
                $data['wherehas'][] = [
                    'relation' => 'follows',
                    'target' => [
                        [
                            'column' => "follower",
                            "operator" => "=",
                            "value" => auth()->user()->id,
                        ],
                    ],
                ];
            }

     
         $result = $this->fetchGeneric($data, $this->follow);
        


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Followers are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved You Follow",
            "description" => "parameter either with post method",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    } 

    public function hasproduct($data = [])
    {
        $meta_index = "productexist";
        $parameters = [];
        $result = $userObj = Product::where('user_id', auth()->user()->id)->count();
        if($result>0)
        {
            $result = true;
        }
        else{
            $result = false;
        }
            return $this->setResponse([
                "code" => 200,
                "title" => "Successfully Retrieve Has Product",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
    }

    public function messages($data = [])
    {
        $meta_index = "UserRatingModel";
        $messObj = MessagesModel::all();
        $userObj = UserRatingModel::all()->where('id',$data['user_id']);
        $result = [];
        foreach ($userObj as $key => $value) {
                    $user_id = $value->id;
                    array_push($result, "UserRatingModel");
                    array_push($result, $value);
                    foreach ($messObj as $key => $value2) {
                      if($value2->user_id == $user_id )
                      {
                        $prod_id = $value2->id;
                        array_push($result, "messages");
                        array_push($result, $value2);
                     }
            }
        }

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No UserRatingModel are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved UserRatingModel->product->category",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);
    }

   /* public function getmessages($data = [])
    {
        $parameters = [];
        $count = 0;
        $meta_index = "messages";
        $a = [];
        $a[0] = $data['sender_id'];
        $a[1] = $data['reciever_id'];
        
        $b[0] = $data['reciever_id'];
        $b[1] = $data['sender_id'];
        $messObj1 = MessagesModel::orderBy('created_at', 'desc')->whereIn('reciever_id',$a)->whereIn('sender_id',$b)->get();
        $result = [];

    
                    foreach ($messObj1 as $key => $value2) {
                        array_push($result, $value2);
                    }

                


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Messages found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved Messages",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);
    } */
 public function getmessages($data = [])
    {
        $meta_index = "messages";
        $parameters = [];
        $count = 0;
        $data['where'] = [
            [
                "target" => "sender_id",
                "operator" => "=",
                "value" => $data['sender_id'],
            ],
            [
                "target" => "reciever_id",
                "operator" => "=",
                "value" =>  $data['reciever_id'],
            ],
        ];
      
        $result = $this->fetchGeneric($data, $this->messages);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Messages found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved Messages",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);
    }

    public function getProductRate($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['relations'] = ["ratings"];
       
        $data['single'] = true;
        $data['where'] = [
            [
                "target" => "user_id",
                "operator" => "=",
                "value" =>  auth()->user()->id,
            ],
        ];

        $result = $this->fetchGeneric($data, $this->products);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No product rate found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved rate",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);
    }
    
    public function getReviews($data = [])
    {
        $meta_index = "UserRatingModel";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['relations'] = ["reviews"];
       
        $data['single'] = true;
        $data['where'] = [
            [
                "target" => "id",
                "operator" => "=",
                "value" =>  auth()->user()->id,
            ],
        ];

        $result = $this->fetchGeneric($data, $this->UserRatingModel);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No reviews found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully reviews",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);
    }
    public function create($data = [])
    {

            
            $userrating = $this->userrating->init($this->userrating->pullFillable($data));
            $userrating->save($data);
           
            

            if (!$userrating->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $userrating->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully create user rating.",
                "parameters" => $userrating,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }
        $user = $this->UserRatingModel->find($data['id']);
        if($user==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }
    
        if(isset($data["image"])){   
                $url = $user->image_url;
                $file_name = basename($url);
                Storage::delete('images/' . $file_name);
                define('UPLOAD_DIR', 'storage/images/');
                $file = request()->image->move(UPLOAD_DIR, $data['image']);
                $url = asset($file);
                $data['image_url'] = $url;
        }

        $user->save($data);
        if (!$user->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $user->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a User.",
            "meta"        => [
                "status" => $user,
            ]
        ]);
            
        
    }
    
    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $user = $this->UserRatingModel->find($data['id']);
        if($user==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "User not found.",
            ]);
        }
        
        if (!$user->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $user->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a User.",
            "meta"        => [
                "status" => $user,
            ]
        ]);
            
        
    }

    public function search($data = [])
    {

        $meta_index = "search";
        $parameters = [];
        $count = 0;

        


        $someVariable = $data['search'];
        
        


        $result = DB::select( DB::raw("SELECT DISTINCT p.`id` , p.`name` , p.`created_at` ,
        p.`donate` , p.`product_lat`, p.`location` FROM  product p JOIN delivery d ON p.`id` = d.`product_id` JOIN reservation r ON p.`id` = r.`product_id` 
       WHERE p.`name` LIKE '%$someVariable%'
       OR p.`created_at`  LIKE '%$someVariable%'
       OR p.`product_lng`  LIKE '%$someVariable%'
       OR p.`product_lat`  LIKE '%$someVariable%'
       OR p.`location`  LIKE '%$someVariable%'
       OR p.`donate`  LIKE '%$someVariable%'
        
        "), array(
           'somevariable' => $someVariable
         ));




         if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "Search Not Found!",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->UserRatingModel->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Search Data",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
            
    }

    
    // public function follow($data = [])
    // {
    //     $meta_index = "UserRatingModel";
    //       $parameters = [];
    //     $count = 0;
    //     $data['relations'] = ["follow"];
    //     if (isset($data['user_id']) &&
    //         is_numeric($data['user_id'])) {

    //         $data['single'] = true;
    //         $data['where'] = [
    //             [
    //                 "target" => "id",
    //                 "operator" => "=",
    //                 "value" => $data['user_id'],
    //             ],
    //         ];

    //         $parameters['user_id'] = $data['user_id'];

    //     }

    //     $count_data = $data;

    //     $result = $this->fetchGeneric($data, $this->UserRatingModel);

    //     if (!$result) {
    //         return $this->setResponse([
    //             'code' => 404,
    //             'title' => "No agents are found",
    //             "meta" => [
    //                 $meta_index => $result,
    //             ],
    //             "parameters" => $parameters,
    //         ]);
    //     }

    //     // $count = $this->countData($count_data, refresh_model($this->UserRatingModel->getModel()));

    //     return $this->setResponse([
    //         "code" => 200,
    //         "title" => "Successfully retrieved followers",
    //         "meta" => [
    //             $meta_index => $result,
    //             // "count" => $count,
    //         ],
    //         "parameters" => $parameters,
    //     ]);
    // }



}
