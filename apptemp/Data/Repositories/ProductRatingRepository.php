<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\ProductRatingModel;
use App\Data\Repositories\BaseRepository;
use App\Approval;
use App\Data\Models\Users;
use App\Data\Models\MessagesModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ProductModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Models\ImageModel;

use App\User;
use App\Image;
use App\Reports;
use App\productratings;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ProductRatingRepository extends BaseRepository
{

    protected $approval;
    protected $productrating;
    protected $product;

    public function __construct(ProductRatingModel $productrating,ProductModel $product) 
    {
        $this->productrating = $productrating;
        $this->product = $product;
    }

    public function fetchproductrating($data = [])
    {
        $meta_index = "productrating";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "productrating";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->productrating);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        
          
            // if (!isset($data['reserved_id'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "reserved_id is not set.",
            //     ]);
            // }
            // if (!isset($data['lack_of_approved'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "reserved_id is not set.",
            //     ]);
            // }
            // if (!isset($data['rejected'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "rejected is not set.",
            //     ]);
            // }
            // if (!isset($data['approved'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "approved is not set.",
            //     ]);
            // }

       

        //     $productrating = $this->productrating->init($this->productrating->pullFillable($data));
        //     $productrating->save($data);


        // $meta_index = "users";

       
        // $userObj = Users::all();
        // $prodObj = productratingModel::all()->where('id',$data['reserved_id']);
        // $result = [];
        // foreach ($userObj as $key => $value) {
        //             $user_id = $value->id;
                   
                   
        //             foreach ($prodObj as $key => $value2) {
        //               if($value2->user_id == $user_id)
        //               {
        //                 array_push($result, $value);
        //                 $prod_id = $value2->id;
        //                // array_push($result, "productrating");
        //               //  array_push($result, $value2);
        //               }
        //             }
        //     }
            
            
        //     $a["Users"] = $result;
        //     $a["Request"][] = $productrating;
        $productrating = $this->productrating->init($this->productrating->pullFillable($data));
        
        $productrating->save($data);
        if (!$productrating->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $productrating->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a productrating.",
            "meta"        => [
                "status" => $productrating,
            ]
        ]);
        
    }

    public function update($data = [])
    {
        $productrating->save($data);
        if (!$productrating->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $productrating->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a productrating.",
            "meta"        => [
                "status" => $productrating,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $approval = $this->approval->find($data['id']);
        if($approval==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$approval->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $approval->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a approval.",
            "meta"        => [
                "status" => $approval,
            ]
        ]);
            
        
    }


    public function getProductRating($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['single'] = true;
        $data['where'] = [
            [
                "target" => "product_id",
                "operator" => "=",
                "value" =>  $data['product_id'] ,
            ],
        ];

        $result = $this->fetchGeneric($data, $this->productrating);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No product rate found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved rate",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);
    }


}
