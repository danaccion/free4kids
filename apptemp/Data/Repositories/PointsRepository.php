<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\pointsModel;
use App\Data\Repositories\BaseRepository;
use App\Approval;
use App\Data\Models\Users;
use App\Data\Models\MessagesModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ReservationModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Models\ImageModel;
use App\User;
use App\Image;
use App\Reports;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class PointsRepository extends BaseRepository
{

    protected $points;
    protected $reservation;

    public function __construct(PointsModel $points,
    ReservationModel $reservation) 
    {
        $this->points = $points;
        $this->reservation = $reservation;
    }

    public function fetchpoints($data = [])
    {
        $meta_index = "points";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "points";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->points);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
      
        if (!isset($data['notif_type_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "notif_type_id is not set.",
            ]);
        }

        if (!isset($data['action'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "action is not set.",
            ]);
        }

        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }


        if (!isset($data['description'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "description is not set.",
            ]);
        }

        $points = $this->points->init($this->points->pullFillable($data));
        $points->save($data);
        if (!$points->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $points->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully create points.",
            "meta"        => [
                "status" => $points,
            ]
        ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['reserved_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reserved_id is not set.",
            ]);
        }
        
        if (!isset($data['approved'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approved is not set.",
            ]);
        }

        if (!isset($data['rejected'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "rejected is not set.",
            ]);
        }
        $points = $this->points->find($data['id']);
        if($points==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $points->save($data);
        if (!$points->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $points->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a points.",
            "meta"        => [
                "status" => $points,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $points = $this->points->find($data['id']);
        if($points==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$points->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $points->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a points.",
            "meta"        => [
                "status" => $points,
            ]
        ]);
            
        
    }


    public function getPoints($data = [])
    {
        $meta_index = "points";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        // $data['relations'] = ["reviews"];
       
        $data['single'] = true;
        $data['where'] = [
            [
                "target" => "user_id",
                "operator" => "=",
                "value" =>  auth()->user()->id,
            ],
        ];

        $result = $this->fetchGeneric($data, $this->points);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No reviews found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully points",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);
    }

}
