<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class followModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'follow';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','status','follower'
    ];

    protected $hidden = [
        'deleted_at','updated_at','created_at'
        
    ];

    public function follow()
    {
        return $this->hasMany('\App\Data\Models\followModel', 'id', 'id')->where('user_id', auth()->user()->id);
    }

    public function follows()
    {
        return $this->hasMany('\App\Data\Models\followModel', 'id', 'id');
    }

    public function Usersid()
    {
        return $this->hasMany('\App\Data\Models\Users', 'id', 'user_id');
    }


    public function Followersid()
    {
        return $this->hasMany('\App\Data\Models\Users', 'id', 'follower');
    }
}
