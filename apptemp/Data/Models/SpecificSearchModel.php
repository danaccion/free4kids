<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class SpecificSearchModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'specific_search';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','feel','able','shipping'
    ];
    protected $hidden = [
        
    ];

}
