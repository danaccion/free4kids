<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class DelsaveModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'product_id';
    protected $table = 'saved_products';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','status','user_id','product_id'
    ];

   
}
