<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class ClosedDealModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'product_id';
    protected $table = 'closed_deal';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id','buyer_id','seller_id'
    ];

    public function Buyer()
    {
        return $this->hasMany('\App\Data\Models\Users', 'id', 'buyer_id')->where('id', auth()->user()->id);;
    }


    public function Seller()
    {
        return $this->hasMany('\App\Data\Models\Users', 'id', 'seller_id');
    }

   
}
