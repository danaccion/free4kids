<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class MessagesModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'messages';
    protected $appends = [
       'user'
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'createdAt','image_url', 'location_lat','location_long','messages','approval_id','sender_id','receiver_id','name','time','seen','text','_id'
    ];


    public function getUserAttribute(){    
        $arrayobj = ['_id'=> (int)$this->sender_id];
  
        return  $arrayobj;
    }


   
}
