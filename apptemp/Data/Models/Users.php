<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;
use App\User;
use App\Data\Models\UserRatingModel;
class Users extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'users';
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

 
   
    protected $fillable = [
        'name', 'email', 'password','user_name','image_url','reports','id'
    ];

    protected $hidden = [
        'created_at','password','email_verified','updated_at','deleted_at','email_verified_at','remeber_token','image_id','laravel_through_key'
        
    ];

	
    protected $appends = [
        'AverageRating'
     ];
     public function getAverageRatingAttribute(){    
        $arrayobj = ['user_id'=> $this->id];
        $result = UserRatingModel::where('user_id',$this->id)->sum('rate');;
        $count = UserRatingModel::where('user_id',$this->id)->count('id');;
        if($count == 0){
            return 0;
        }else if($result == 0)
        {
            return 0;
        }
        $result = $result / $count;
        
        return   $result;
    }

    // protected $searchable = [
    //     'user_info.firstname',
    //     'user_info.middlename',
    //     'user_info.lastname',
    //     'id',
    //     'uid',
    //     'email', 
    //     'password', 
    //     'access_id',
    //     'loginFlag',
    //     'company_id'
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    public function reports()
    {
        return $this->hasMany('\App\Data\Models\ReportsModel', 'user_id', 'id');
    }

    public function reviews()
    {
        return $this->hasMany('\App\Data\Models\ReviewsModel', 'user_id', 'id');
    }

    public function location()
    {
        return $this->hasMany('\App\Data\Models\Location', 'user_id', 'id');
    }

    public function products()
    {
        return $this->hasMany('\App\Data\Models\Product', 'user_id', 'id');
    }

    public function productsid()
    {
        return $this->hasMany('\App\Data\Models\Product', 'id', 'id');
    }


    public function reservation()
    {
        return $this->hasMany('\App\Data\Models\ReservationModel', 'product_id', 'id');
    }

 

    public function approval()
    {
        return $this->hasMany('\App\Data\Models\ApprovalModel', 'user_id', 'id');
    }

    public function messages()
    {
        return $this->hasMany('\App\Data\Models\MessagesModel', 'user_id', 'id');
    }

    public function category()
    {
        return $this->hasMany('\App\Data\Models\ProductCategory', 'id', 'id');
    }

    
    public function delivery()
    {
        return $this->hasMany('\App\Data\Models\DeliveryModel', 'user_id', 'id');
    }

    public function follow()
    {
        return $this->hasMany('\App\Data\Models\followModel', 'user_id', 'id')->where('user_id', auth()->user()->id);;
    }


    public function userrating()
    {
        return $this->hasMany('\App\Data\Models\UserRatingModel', 'user_id', 'id');
    }

    public function users()
    {
        return $this->hasMany('\App\Data\Models\Users', 'id', 'id');
    }
    public function author()
    {
        return $this->hasManyThrough(
            '\App\Data\Models\Users',
            '\App\Data\Models\UserRatingModel',
            'user_id', // Foreign key on users table...
            'id' // Foreign key on posts table...
        );
    }


    public function productrating()
    {
        return $this->hasMany('\App\Data\Models\ProductRatingModel', 'product', 'id');
    }


    // public function user_info() {
    //     return $this->hasOne('\App\Data\Models\UserInfo', 'id', 'uid');
    // }

    // public function user_logs() {
    //     return $this->hasMany('\App\Data\Models\ActionLogs', 'user_id', 'id');
    // }
    
    // public function accesslevel(){
    //     return $this->hasOne('\App\Data\Models\AccessLevel', 'id', 'access_id');
    // }

    // public function position(){
    //     return $this->belongsTo('\App\Data\Models\AccessLevel', 'access_id', 'id');
    // }

    // public function userdata() {
    //     return $this->belongsTo('\App\Data\Models\UserInfo', 'uid', 'id');
    // }

    // public function accesslevelhierarchy(){
    //     return $this->hasOne('\App\Data\Models\AccessLevelHierarchy', 'child_id', 'id');
    // }

    // public function getNameAttribute(){
    //     $name = null;
    //     if(isset($this->user_info)){
    //         $name = $this->user_info->full_name;
    //     }
        
    //     return $name;
    // }

    // public function getValueAttribute(){
    //     $name = null;
    //     if(isset($this->user_info)){
    //         $name = $this->user_info->id;
    //     }
        
    //     return $name;
    // }

    public function getUserid()
    {
        $id = null;
        if (isset($this->user)) {
            $id = $this->user->id;
        }

        return $id;
    }
}
