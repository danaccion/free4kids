<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class DeliveryModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'delivery';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'location_id','product_id','pay'
    ];

    public function User()
    {
        return $this->belongsTo('\App\Data\Models\Users', 'user_id');
    }
   
}
