<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class PointsModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'points';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'notif_type_id','action','user_id','description'
    ];

   
}
