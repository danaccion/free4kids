<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Data\Repositories\MessagesRepository;
use App\Data\Models\MessagesModel;

class MessagesController extends BaseController
{
   protected $messages;

    public function __construct(
        MessagesRepository $messages
    ){
        $this->messages = $messages;
    }
    public function index(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->messages->fetchMessages($data))->json();
    }

   

    public function create(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->messages->create($data))->json();
    }
    
    public function update(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->messages->update($data))->json();
    }

    public function updateSeen(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->messages->updateSeen($data))->json();
    }

    public function delete(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->messages->delete($data))->json();
    }
    
    // public function search(Request $request)
    // {
       
    //     $data = $request->all();
    //     return $this->absorb($this->action_logs->search($data))->json();
    // }
    // /** 
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */

    // public function create(Request $request)
    // {
    //     $data = $request->all();
    //     return $this->absorb($this->action_logs->logsInputCheck($data))->json();     
    // }

    
    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function log(Request $request, $id)
    // {
    //     $data['id'] = $id;

    //     if (!isset($data['id']) ||
    //         !is_numeric($data['id']) ||
    //         $data['id'] <= 0) {
    //         return $this->setResponse([
    //             'code'  => 500,
    //             'title' => "Schedule ID is invalid.",
    //         ]);
    //     }

    //     return $this->absorb($this->action_logs->fetchUserLog($data))->json();
    // }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
