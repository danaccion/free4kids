<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Data\Repositories\UsersRepository;
use App\Data\Repositories\ProductRepository;
use App\Data\Models\Users;
use App\Data\Models\Product;

class UserController extends BaseController
{
   protected $users;

    public function __construct(
        UsersRepository $users,
        ProductRepository $product
    ){
        $this->users = $users;
        $this->product = $product;
    }
    public function index(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->fetchUser($data))->json();
    }

    public function location(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->location($data))->json();
    }

    public function hasproduct(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->hasproduct($data))->json();
    }

    public function followers(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->followers($data))->json();
    }

    public function you_follow(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->you_follow($data))->json();
    }


    public function saved_products(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->saved_products($data))->json();
    }

    public function products(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->products($data))->json();
    }

    public function product_category(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->product_category($data))->json();
    }

    public function reports(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->reports($data))->json();
    }

    public function reviews(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->reviews($data))->json();
    }

    public function reservation(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->reservation($data))->json();
    }

    public function approval(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->approval($data))->json();
    }

    public function all_product_category(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->all_product_category($data))->json();
    }

    

    public function messages(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->messages($data))->json();
    }


    public function create(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->create($data))->json();
    }
    
    public function update(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->update($data))->json();
    }

    public function delete(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->delete($data))->json();
    }

    public function search(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->search($data))->json();
    }
    
    public function getmessages(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->getmessages($data))->json();
    }
    
    public function set_category_id(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->set_category_id($data))->json();
    }

    public function searchProduct(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->searchProducts($data))->json();
    }

    public function getProductRate(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->getProductRate($data))->json();
    }
    
    public function getReviews(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->getReviews($data))->json();
    }

    public function getUserRating(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->getUserRating($data))->json();
    }


    public function getProductRating(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->users->getProductRating($data))->json();
    }




    // public function search(Request $request)
    // {
       
    //     $data = $request->all();
    //     return $this->absorb($this->action_logs->search($data))->json();
    // }
    // /** 
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */

    // public function create(Request $request)
    // {
    //     $data = $request->all();
    //     return $this->absorb($this->action_logs->logsInputCheck($data))->json();     
    // }

    
    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function log(Request $request, $id)
    // {
    //     $data['id'] = $id;

    //     if (!isset($data['id']) ||
    //         !is_numeric($data['id']) ||
    //         $data['id'] <= 0) {
    //         return $this->setResponse([
    //             'code'  => 500,
    //             'title' => "Schedule ID is invalid.",
    //         ]);
    //     }

    //     return $this->absorb($this->action_logs->fetchUserLog($data))->json();
    // }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
