<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\BaseController;
use Intervention\Image\ImageManagerStatic as Image;
require '../vendor/autoload.php';
class RegisterController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $image_url=null;
        if(isset($request['image'])){
            request()->validate([

                'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',
    
            ]);
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            define('UPLOAD_DIR', 'storage/images/');
            $resize = Image::make($request['image'])->resize(400,400)->encode('jpg');   
            $file =  $resize->save(UPLOAD_DIR.$imageName,60);
            $url= asset(UPLOAD_DIR.$imageName);
            $image_url = $url;  
         
        }
       
        
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'user_name' => $request['user_name'],
            'image_url' => $image_url,
            'password' => Hash::make($request['password']),
        ]);
       
       return $this->setResponse([
            "code" => 200,
            "title" => "Successfully added A User.",
            "meta" => [
                'name' => $request['name'],
                'email' => $request['email'],
                'user_name' => $request['user_name'],
                'image_url' => $image_url,
            ],
        ])->json();
    }
}
