<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Reservations;
use App\Approval;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

//pwede nimo iimport dri ang mga model preha ra imong repositories
use Carbon\Carbon;
use DB;
class deletereserve extends Command
{
    use SoftDeletes;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:deleteReserve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        //dri ang logic sa function
        // $res = Approval::
        $users = DB::table('reservation')
            ->Join('approval', 'reservation.id', '!=', 'approval.reserved_id')
            ->where('reservation.created_at', '>',Carbon::now()->subDays(2));

            
            if($updateActiveProducts = DB::table('reservation')
            ->Join('product', 'product.id', '=', 'reservation.product_id')
            ->where('reservation.created_at', '>',Carbon::now()->subDays(2))
            ->update([ 'product.status' =>'inactive' ])){
                echo json_encode("deactivated product");
            }


           


        if(!$users->delete()){
            echo json_encode("All Clean");
            return;
        }

          echo json_encode("Successfully delete reservation");
    }   
    
        
}
