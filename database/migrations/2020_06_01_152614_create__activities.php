<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('notif_type_id')->nullable();
            $table->unsignedInteger('action')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('receiver_id')->nullable();
            $table->string('description')->nullable();
            $table->unsignedInteger('read')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_activities');
    }
}
