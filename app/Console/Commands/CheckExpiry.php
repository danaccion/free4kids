<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Reservations;
class CheckExpiry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:checkexpiry';

    /**
     * The console command description.
     * 
     * @var string
     */
    protected $description = 'Checking.!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $userObj = Reservation::all()->where('id',1);
         
        echo json_encode( $userObj);
    }
}
