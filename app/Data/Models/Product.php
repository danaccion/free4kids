<?php

namespace App\Data\Models;
use App\Data\Models\ImageModel;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;
use Illuminate\Support\Arr;
use App\Image;
use DB;

class Product extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'product';
    protected $appends = [
       'reserved','save','approved','request','images','contactnumber','closed'
    ];

     public $status_reservation = [
        '1' => 'YES',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','size','category_id','name', 'user_id','id','location','product_lat','product_lng','image_id','age','sex','brand','stand','description','expiry_date',
        'delivery_type','active','image_url','status','upload_status','courrier','contact_approval'
        
    ];

    
    protected $hidden = [
        'reservation','saved_products','image','deleted_at','updated_at','created_at','expiry_date','contact'
        
    ];

    

    protected $searchable = [
        'name','description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */ 
     public function getReservedAttribute(){    
        $name = 'No';
        if(isset($this->reservation[0]->id )){
            $name =  "Yes";
			
			
			    ($updateReservationProducts = DB::table('product')
            ->where('product.id', '=', $this->id)
            ->update([ 'product.reservationexclude' =>'x' ])) ? : "Cant Update exclude product reservation";
        }
        
        return  $name;
    }
    
    
    public function getClosedAttribute(){    
        $name = 'No';
        if(isset($this->closed_deal[0]->id )){
            $name =  "Yes";

            ($closed = DB::table('product')
            ->where('product.id', '=', $this->id)
            ->update([ 'product.closed' =>'x' ])) ? : "Cant Update closed";
        }
        return  $name;
    }
	
	
    public function getApprovedAttribute(){    
        $name = 'Pending';
        if(isset($this->reservation[0]->id)){
        if($this->reservation[0]->approved==1){
            $name =  "Yes";
            }else if($this->reservation[0]->approved==0){
                $name = 'No';
            }           
        }
        return  $name;
    }

    public function getImagesAttribute(){    
        // $image = null;
        // if(isset($this->image)){
            

        // }
        // return   $image;
      
        $image=Arr::pluck( $this->image, 'image_url');
  
        return $image;
    }

  public function getSaveAttribute(){    
        $name = 'No';
        if(isset($this->saved_products[0]->id )){
            $name =  "Yes";
        }
        
        return  $name;
    }


    public function getContactNumberAttribute(){    
        $name = 'No';
        if(isset($this->contact[0]->user_id )){
            $name = 'yes';
            $name=Arr::pluck( DB::table('contact')
            ->where('user_id', '=', $this->contact[0]->user_id)
            ->get(), 'number');
        }
        
        return  $name;
    }

    public function getRequestattribute(){    
        $name = 'No';
        if(isset($this->request_contact[0]->user_id ) 
        & isset($this->request_contact[0]->product_id )){
            $name = 'approve';
        }
        else if(!isset($this->request_contact[0]->user_id ) 
        & isset($this->request_contact[0]->product_id )){
            $name = 'pending';
        }
        return  $name;
    }
    
    
    public function reservation_status1()
    {
        return $this->hasMany('\App\Data\Models\ReservationModel', 'product_id','id');
    }

    public function images()
    {
        return $this->hasMany('\App\Data\Models\ImageModel', 'product_id','id');
    }

    public function Users()
    {
        return $this->hasMany('\App\Data\Models\Users', 'id','user_id');
    }
    public function category()
    {
        return $this->hasMany('\App\Data\Models\ProductCategory', 'id', 'category_id');
    }
    public function reports()
    {
        return $this->hasMany('\App\Data\Models\ReportsModel', 'user_id', 'id');
    }

    public function product_reports()
    {
        return $this->hasMany('\App\Data\Models\ReportsModel', 'product_id', 'id');
    }


    public function reviews()
    {
        return $this->hasMany('\App\Data\Models\ReviewsModel', 'product_id', 'id');
    }

    public function maxProducts()
    {
        return $this->hasMany('\App\Data\Models\Product', 'user_id', 'user_id');
    }

    public function location()
    {
                return $this->hasManyThrough(
                    '\App\Data\Models\Location',
                    '\App\Data\Models\DeliveryModel',
                    'product_id', // Foreign key on users table...
                    'delivery_id', // Foreign key on posts table...
                    'id', // Local key on countries table...
                    'id' // Local key on users table...
                );
    }

    public function located()
    {
        return $this->hasOne('\App\Located', 'user_id');
    }

    public function image()
    {
        return $this->hasMany('\App\Data\Models\ImageModel', 'product_id', 'id');
    }
	
	   public function hasoneimage()
    {
        return $this->hasMany('\App\Data\Models\ImageModel', 'product_id', 'id');
    }

    public function delivery()
    {
        return $this->hasMany('\App\Data\Models\DeliveryModel', 'product_id', 'id');
    }

    // public function ()
    // {
    //     return $this->hasMany('\App\Data\Models\Model', 'user_id', 'id');
    // }
    public function product_category()
    {
        return $this->hasMany('\App\Data\Models\ProductCategory', 'id', 'id');
    }

    public function productrating()
    {
        return $this->hasMany('\App\Data\Models\ProductRatingModel', 'product_id', 'id');
    }
  
    
    public function product()
    {
        return $this->hasMany('\App\Data\Models\Product', 'user_id', 'id');
    }

    public function products()
    {
        return $this->hasMany('\App\Data\Models\Product', 'user_id', 'user_id');
    }
    public function productsid()
    {
        return $this->hasMany('\App\Data\Models\Product', 'id', 'id');
    }
    
    public function productstocategory()
    {
        return $this->hasMany('\App\Data\Models\Product', 'category_id', 'category_id');
    }

  

    
    public function reservation()
    {
        return $this->hasMany('\App\Data\Models\ReservationModel', 'product_id', 'id');
    }

    public function closed_deal()
    {
        return $this->hasMany('\App\Data\Models\ClosedDealModel', 'product_id', 'id');
    }

    public function reservation_approval()
    {
        return $this->hasManyThrough(
            '\App\Data\Models\ApprovalModel',
            '\App\Data\Models\ReservationModel',
            'product_id', // Foreign key on ApprovalModel table...
            'reserved_id' // Foreign key on ReservationModel table...
        );
    }

    
    public function approval()
    {
        return $this->hasMany('\App\Data\Models\ApprovalModel', 'reserved_id', 'reserved_id');
    }

    public function saved_products()
    {
        return $this->hasMany('\App\Data\Models\SaveModel', 'product_id', 'id')->where('user_id', auth()->user()->id);
    }
    
    public function messages()
    {
        return $this->hasMany('\App\Data\Models\MessagesModel', 'user_id', 'id');
    }

    public function contact()
    {
        return $this->hasMany('\App\Data\Models\ContactModel', 'user_id');
    }

    public function request_contact()
    {
        return $this->hasMany('\App\Data\Models\RequestContactModel', 'user_id','user_id');
    }
    // public function user_logs() {
    //     return $this->hasMany('\App\Data\Models\ActionLogs', 'user_id', 'id');
    // }
    
    // public function accesslevel(){
    //     return $this->hasOne('\App\Data\Models\AccessLevel', 'id', 'access_id');
    // }

    // public function position(){
    //     return $this->belongsTo('\App\Data\Models\AccessLevel', 'access_id', 'id');
    // }

    // public function userdata() {
    //     return $this->belongsTo('\App\Data\Models\UserInfo', 'uid', 'id');
    // }

    // public function accesslevelhierarchy(){
    //     return $this->hasOne('\App\Data\Models\AccessLevelHierarchy', 'child_id', 'id');
    // }

    // public function getNameAttribute(){
    //     $name = null;
    //     if(isset($this->user_info)){
    //         $name = $this->user_info->full_name;
    //     }
        
    //     return $name;
    // }

    // public function getValueAttribute(){
    //     $name = null;
    //     if(isset($this->user_info)){
    //         $name = $this->user_info->id;
    //     }
        
    //     return $name;
    // }


    
    public function products_user_id()
    {
        return $this->hasMany('\App\Data\Models\Product', 'user_id', 'id');
    }

    public function ratings()
    {
        return $this->hasMany('\App\Data\Models\RatingsModel', 'product_id', 'id');
    }
}
