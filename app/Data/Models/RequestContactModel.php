<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class RequestContactModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'request_contact';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','product_id','request_id','description','owner_approved'
    ];

    

   
}
