<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class ReservationModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'reservation';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id','undo','redo','expiry','approved','user_id','status','owner_id'
    ];
    protected $hidden = [
        'deleted_at','updated_at','created_at'
        
    ];
	

	
	
    public function products()
    {
        return $this->hasMany('\App\Data\Models\Product', 'id', 'product_id');
    }

    public function users()
    {
        return $this->hasOne('\App\Data\Models\Users', 'id', 'owner_id');
    }

    public function reservation()
    {
        return $this->hasMany('\App\Data\Models\ReservationModel', 'product_id', 'id');
    }

    public function reservation_expiry()
    {
        return $this->hasMany('\App\Data\Models\ReservationModel', 'product_id', 'product_id');
    }


    public function reservationwithUserid()
    {
        return $this->hasMany('\App\Data\Models\ReservationModel', 'id', 'id')->where('user_id', auth()->user()->id);
    }

    public function approval()
    {
        return $this->hasMany('\App\Data\Models\ApprovalModel', 'reserved_id', 'id')->where('approved','1');;
    }
   
    public function approval_product()
    {
        return $this->hasMany('\App\Data\Models\Product', 'id', 'product_id');
    }

    public function approval_user()
    {
        return $this->hasOne('\App\Data\Models\Users', 'id', 'user_id');
    }


}
