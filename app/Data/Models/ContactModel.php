<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class ContactModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'user_id';
    protected $table = 'contact';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','number'
    ];

    
    public function Location()
    {
        return $this->hasOne('\App\Data\Models\Location','user_id');
    }

    public function products()
    {
        return $this->hasMany('\App\Data\Models\Product', 'user_id', 'user_id');
    }
}
