<?php

namespace App\Data\Models;
use App\User;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;
use Intervention\Image\ImageManagerStatic as Image;
require '../vendor/autoload.php';
class MessagesDeleteById extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'user_id';
    protected $table = 'deletemessagetable';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'receiver_id','date','user_id'
    ];

}
