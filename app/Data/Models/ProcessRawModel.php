<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class ProcessRawModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'process_raw';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name'
    // ];


    public function getPointsAttribute(){    
        $arrayobj = ['seller_id'=> $this->id];
        $result = PointsModel::where('seller_id',$this->id)->count('id');;
        return   $result;
    }

    public function Users()
    {
        return $this->hasMany('\App\Data\Models\Users', 'id','user_id');
    }
   
}
