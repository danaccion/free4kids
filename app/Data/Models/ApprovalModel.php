<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class ApprovalModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'approval';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reserved_id','approved','rejected','lack_of_approved'
    ];

   
}
