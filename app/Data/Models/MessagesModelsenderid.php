<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;
use App\User;
use App\Data\Models\MessagesModel;

class MessagesModelsenderid extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'sender_id';
    protected $table = 'messages';
    protected $appends = [
       'user'
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_id','image_url', 'location_lat','location_long','text','approval_id','sender_id','reciever_id','name','time','seen'
    ];

    public function Users()
    {
        return $this->hasMany('\App\Data\Models\Users', 'id','sender_id');
    }

    public function Message()
    {
        return $this->hasMany('\App\Data\Models\MessagesModel', 'id','sender_id');
    }

    public function getUserAttribute(){
        $user = MessagesModel::find($this->Users[0]->id);;
         return $user;
  
 }
   
}
