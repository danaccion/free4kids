<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class DelReservationModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'product_id';
    protected $table = 'reservation';
    // protected $appends = [
    //    'value','name'
    // ];

    protected $hidden = [
        'deleted_at','updated_at','created_at','expiry'
        
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id','undo','redo','user_id'
    ];

    public function products()
    {
        return $this->hasMany('\App\Data\Models\Product', 'user_id', 'id');
    }

    public function reservation()
    {
        return $this->hasMany('\App\Data\Models\ReservationModel', 'product_id', 'id');
    }
    public function approval()
    {
        return $this->hasMany('\App\Data\Models\ApprovalModel', 'reserved_id', 'id');
    }
   
}
