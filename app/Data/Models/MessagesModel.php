<?php

namespace App\Data\Models;
use App\User;
use App\Data\Models\MessagesDeleteById;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;
use Intervention\Image\ImageManagerStatic as Image;
require '../vendor/autoload.php';
class MessagesModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'messages';
    protected $appends = [
       'user','screen','name','massdelete'
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'createdAt','image_url', 'location_lat','location_long','messages','approval_id','sender_id','receiver_id','name','time','seen','text','_id','type'
    ];


    public function getUserAttribute(){    
        $arrayobj = ['_id'=> (int)$this->sender_id];
  
        return  $arrayobj;
    }


    public function getScreenAttribute(){    
        $view = '';
        if(strcasecmp($this->type, 'buyer') == 0)
        {
            $view = 'ReservationChat';
        }else{
            $view = 'ApproveChat';
        }
        return  $view ;
    }

    public function getNameAttribute(){    
        $arrayobj = ['seller_id'=> $this->id];
        $result = User::where('id',$this->sender_id)->pluck('name');
        return   $result[0];
    }

    public function getMassDeleteAttribute(){    
        $arrayobj = ['seller_id'=> $this->id];
        $result = MessagesDeleteById::where('user_id',$this->sender_id)->where('receiver_id',$this->receiver_id)
        ->where('date','<',$this->updated_at)->pluck('date');
        return   $result;
    }

   

}
