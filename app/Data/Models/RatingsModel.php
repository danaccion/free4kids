<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class RatingsModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'ratings';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id','rate','rate_id','comment'
    ];

   
}
