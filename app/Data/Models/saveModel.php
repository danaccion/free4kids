<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;
use DB;

class SaveModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'saved_products';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  protected $fillable = [
        'id','status','user_id','product_id','expiry'
    ];
    protected $hidden = [
        'deleted_at','updated_at','created_at','laravel_through_key','AverageRating'
        
    ];

    protected $appends = [
        'Save'
     ];
     

    public function getSaveAttribute(){    
            (DB::table('saved_products')
            ->where('product_id', '=', $this->product_id)
            ->where('user_id', '=', $this->user_id)
            ->update([ 'owner_id' =>$this->products[0]->user_id])) ? : "Cant Update exclude saved products";
             return $this->products[0]->user_id;
    }

    
   public function products()
    {
        return $this->hasMany('\App\Data\Models\Product', 'id', 'product_id');
    }
  
    public function users()
    {
        return $this->hasOne('\App\Data\Models\Users', 'id', 'owner_id');
    }
}
