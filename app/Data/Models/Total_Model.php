<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class Total_Model extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'total_order';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id','undo','redo','expiry','approved','user_id','status','owner_id'
    ];

   
}
