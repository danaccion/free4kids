<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;
use App\Data\Models\Points;
use App\Data\Models\MessagesModelsenderid;
use App\Data\Models\ClosedDealModel;
use App\Data\Models\Product;
use App\Data\Models\RawData_Model;
class RawData_Model extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'raw_data';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $hidden = [
      'reserved_id','message_id','bonus_id'
,'approval_id','rejection_id','deactivate_id','follower_id','name','owner_id','user_id'
,'changedat','review_id','sender_id','receiver_id','deleted_at'
,'updated_at','read','productname','closeddeal','points','notif'
         
    ];  
    protected $fillable = [
        // 'id','image_url','user_id','product_id'
        'id','read'
    ];


    protected $appends = [
        // 'text'   
        'productname','closeddeal','points',//'Follower'
    ];

    public function getFollowerAttribute(){    
        $result = RawData_Model::where('user_id',$this->follower_id)
        ->where('type' , 'add_product');
        return  $result->get() ;
    }
  
#POINTS
    public function getPointsAttribute(){    
        // $arrayobj = ['user_id'=> $this->receiver_id];
        $result = 0;
        if($this->product_id != null && $this->type != null){
        $result = Points::where('seller_id',$this->user_id)->count();
        }
        return  $result ;
    }
#PRODUCT NAME
      public function getProductNameAttribute(){    
        // $arrayobj = ['user_id'=> $this->receiver_id];
        $result = null;
        if($this->product_id != null){
        $result = Product::where('id',$this->product_id)->pluck('name');
        }
        return  $result;
    }
#CLOSED DEAL
    public function getClosedDealAttribute(){    
        // $arrayobj = ['user_id'=> $this->receiver_id];
        $name = null;
        $result = null;
        if($this->product_id != null){
        $result = ClosedDealModel::where('product_id',$this->product_id)->count();
        if($result > 0)
        {
            $name = "Yes";
        }else{
            $name = "No";
        }
        }
        return  $name;
    }

    public function getTextAttribute(){    
        // $arrayobj = ['user_id'=> $this->receiver_id];
        $result = null;
        if($this->receiver_id != null){
        $result = MessagesModelsenderid::where('receiver_id',$this->receiver_id)->pluck('text');
        }
        return    $result;
    }
}
