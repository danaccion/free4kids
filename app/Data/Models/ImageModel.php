<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class ImageModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'image';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at','deleted_at','product_id','id','user_id'
         
    ];  
    protected $fillable = [
        'id','image_url','user_id','product_id'
    ];

   
}
