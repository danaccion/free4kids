<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class ProductModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'user_id';
    protected $table = 'product';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'size','category_id','image_url', 'location_lat','location_long','messages','approval_id','sender','reciever','user_id','age','sex','brand','stand','description'
    ];


    public function total_order()
    {
        return $this->hasMany('\App\Data\Models\Total_Model', 'product_id', 'id');
    }
   
    public function Users()
    {
        return $this->hasMany('\App\Data\Models\Users', 'user_id', 'id');
    }



    public function location()
    {
             return $this->hasManyThrough(
                    '\App\Data\Models\Location',
                    '\App\Data\Models\DeliveryModel',
                    'product_id', // prduct fk on users table...
                    'delivery_id', // delvery fk location...
                    'id', // Local key on countries table...
                    'id' // Local key on users table...
                );
    }


}
