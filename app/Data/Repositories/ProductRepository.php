<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\Product;
use App\Data\Models\DeliveryModel;
use App\Data\Models\ContactModel;
use App\Data\Models\ImageModel;
use App\Data\Repositories\BaseRepository;
use App\Data\Models\TotalPointsModel;
use App\Delivery;
use App\Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
require '../vendor/autoload.php';
use Intervention\Image\ImageManagerStatic as Images;

class ProductRepository extends BaseRepository
{

    protected $product;
    protected $image;
    protected $delivery;
    protected $totalpoints;

    protected $hidden = [
        'deleted_at','updated_at','created_at'

    ];

    public function __construct(Product $product, DeliveryModel $delivery, ImageModel $image,
    TotalPointsModel $totalpoints, ContactModel $contact)
    {
        $this->product = $product;
        $this->delivery = $delivery;
        $this->image = $image;
        $this->total_points = $totalpoints;
        $this->contact = $contact;
    }

    public function fetchProduct($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["location"];
        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "product";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "user_id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->product);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }



    public function productReviews($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["reviews"];
        if (isset($data['product_id']) &&
            is_numeric($data['product_id'])) {

            $meta_index = "product";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['product_id'],
                ],
                [
                    "target" => "user_id",
                    "operator" => "=",
                    "value" => auth()->user()->id,
                ]
            ];
            $parameters['product_id'] = $data['product_id'];
        }else{
            $data['where'] = [
                [
                    "target" => "user_id",
                    "operator" => "=",
                    "value" => auth()->user()->id,
                ]
            ];
        }
        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->product);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }


    public function maxProducts($data = [])
    {
        $meta_index = "totalProducts";
        $parameters = [];
        $count = 0;


        $result = Product::where([['status','=',"active"]
        ,['saveexclude','=',null],['reservationexclude','=',null],['closed','=',null]])->get();

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                //$meta_index => $result,
                 "total" => $result->count(),
            ],
            "parameters" => $parameters,
        ]);
    }


    public function getDistanceViaLatLong($data = [])
    {
        $meta_index = "DistanceViaLatLong";
        $parameters = [];
        $count = 0;


        $result1 = Product::where([['user_id','=',$data['user_id1']]
        ,['id','=',$data['product_id1']]])->get();


        $result2 = Product::where([['user_id','=',$data['user_id2']]
        ,['id','=',$data['product_id2']]])->get();

          // convert from degrees to radians
        $latFrom = deg2rad($result1[0]['product_lng']);
        $lonFrom = deg2rad( $result2[0]['product_lng']);
        $latTo = deg2rad( $result1[0]['product_lat']);
        $lonTo = deg2rad( $result2[0]['product_lat']);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        
        if (!$result1) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        if (!$result2) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));
        $earthRadiusmtr = 6371000;
        $toKr = 1000;
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => ($angle * $earthRadiusmtr)/$toKr
            ],
            "parameters" => $parameters,
        ]);
    }



    public function create($data = [])
    {


			if(isset($data['image'])){
			$image_url=null;
       /*     request()->validate([

                'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',

            ]);*/
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            define('UPLOAD_DIR', 'storage/images/');
            $resize = Images::make($data['image'])->resize(400,400)->encode('jpg');
            $file =  $resize->save(UPLOAD_DIR.$imageName,60);
            $url= asset(UPLOAD_DIR.$imageName);
           $data['image_url'] = $url;
                $image = $this->image->init($this->image->pullFillable($data));
               $image->save($data);
        }
        $data['status'] = 'active';
       if($image->id){
           $data['upload_status'] = "done";
       }

        $product = $this->product->init($this->product->pullFillable($data));
        $product->save($data);

       $productwithobj = Product::where('user_id',$data['user_id'])
       ->orderBy('id', 'desc')
       ->take(1)
       ->get('id');
       $data['product_id'] = $productwithobj[0]->id;



        $image = $this->image->init($this->image->pullFillable($data));
        $image->save($data);


        if (!$product->save($data)) {

            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $product->errors(),
                ],
            ]);
        }
        $point= DB::table('product')->where([

            ['user_id', '=', $data['user_id']],
        ])->get();

        if($point->count() == 3){
                             if($stored = DB::statement('call raw_data(?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?, ?, ?)',[$data['product_id'], NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'product = 3', $data['user_id'], NULL, "product_3", 'this is a product = 3 testing', NULL, NULL,NULL,"
                             du har oprettet de første 3 produkter og får 1 point","No"])){
                                if($tp = DB::table('total_points')
                                ->where('user_id', '=', $data['user_id'])
                                ->increment('points', 1) #in every current point it adds
                                )
                                {
                                    //echo json_encode("stored procedure success & add product points"); #indicators
                                }else{
                                    echo json_encode("error stored");
                                }

                            }else{
                                echo json_encode("failed to perform #storedprocedure on product points ".$stored);
                            }
                        }
                        else if($point->count() == 1){
                            #insert user

                            $data['points'] = 0;
                            $data['bonus'] = 0;
                            $data['user_id'] = $data['user_id']; #user_id = seller_id
                            $tp = $this->total_points->init($this->total_points->pullFillable($data));

                            if (!$tp->save($data)) {
                                // echo json_encode("test"); #indicators
                                // return;
                            }

                        }
        return $this->setResponse([

            "code"       => 200,
            "title"      => "Successfully create product.",
            "meta"        => [
                "product" => $product,
            ]
        ]);

    }


    public function update($data = [])
    {


        // if (!isset($data['id'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "id is not set.",
        //     ]);
        // }

        // if (!isset($data['user_id'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "user id is not set.",
        //     ]);
        // }

        // if (!isset($data['category_id'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "category id is not set.",
        //     ]);
        // }


        //     if (!isset($data['name'])) {
        //         return $this->setResponse([
        //             'code'  => 500,
        //             'title' => "name is not set.",
        //         ]);
        //     }

        //     if (!isset($data['image'])) {
        //         return $this->setResponse([
        //             'code'  => 500,
        //             'title' => "image is not set.",
        //         ]);
        //     }else{
        //         $data['image_url']=null;
        //             request()->validate([
        //                 'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',

        //             ]);
        //             $imageName = time().'.'.request()->image->getClientOriginalExtension();
        //             define('UPLOAD_DIR', 'storage/images/');
        //             $file =  request()->image->move(UPLOAD_DIR,$imageName);
        //             $url= asset($file);
        //             $data['image_url'] = $url;
        //     }

        if(isset($data['id'])){
			$product = $this->product->find($data['id']);
		}
		else{
			 $product = $this->product->find($data['product_id']);
			}
        if($product==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $product->save($data);
        if (!$product->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $product->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a product.",
            "meta"        => [
                "status" => $product,
            ]
        ]);


    }


    public function deactivate($data = [])
    {


        // if (!isset($data['id'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "id is not set.",
        //     ]);
        // }

        // if (!isset($data['user_id'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "user id is not set.",
        //     ]);
        // }

        // if (!isset($data['category_id'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "category id is not set.",
        //     ]);
        // }


        //     if (!isset($data['name'])) {
        //         return $this->setResponse([
        //             'code'  => 500,
        //             'title' => "name is not set.",
        //         ]);
        //     }

        //     if (!isset($data['image'])) {
        //         return $this->setResponse([
        //             'code'  => 500,
        //             'title' => "image is not set.",
        //         ]);
        //     }else{
        //         $data['image_url']=null;
        //             request()->validate([
        //                 'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',

        //             ]);
        //             $imageName = time().'.'.request()->image->getClientOriginalExtension();
        //             define('UPLOAD_DIR', 'storage/images/');
        //             $file =  request()->image->move(UPLOAD_DIR,$imageName);
        //             $url= asset($file);
        //             $data['image_url'] = $url;
        //     }
        $data['status'] = "inactive";
        if(isset($data['id'])){
			$product = $this->product->find($data['id']);
		}
		else{
             $product = $this->product->find($data['product_id']);
             $data['id'] = $data['product_id'];
            }

            if($updateActiveProducts = DB::table('product')
            ->where('product.id', '=',  $data['id'])
            ->update([ 'product.status' =>'inactive' ])){
                echo json_encode("deactivated product");
            }


        if($product==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $product->save($data);
        if (!$product->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $product->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deactivate a product.",
            "meta"        => [
                "status" => $product,
            ]
        ]);


    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $product = $this->product->find($data['id']);
        if($product==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }

        if (!$product->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $product->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a product.",
            "meta"        => [
                "status" => $product,
            ]
        ]);


    }


    public function restore($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $product =
        $this->product
        ::withTrashed()
        ->where('id', $data['id'])
        ->where('deleted_at', '!=' , null)
        ->restore();
        if($product==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }


        if (!$product) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $product->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully restore.",
        ]);


    }

       public function setProductContactApproval($data = [])
     {
        if (!isset($data['id'])) {  
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }   
       
        $product = $this->product->find($data['id']);
        if($product==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product id not found.",
            ]);
        }


        $data['contact_approval'] = 1;
        $contact_approval = $data['contact_approval'];
        $product->save($data);
        if (!$product->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $product->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully Product Contact.",
            "meta"        => [
                "product_contact" => $product,
            ]
        ]);

    }


    public function getProductContact($data = [])
    {
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }   
      
        $meta_index = "contact";
        $parameters = [];
        $count = 0;

        $result  = DB::table('contact')->join('location','location.user_id', '=' ,'contact.user_id')
        ->where('contact.user_id', '=',  $data['user_id'])
        ->distinct()->get();
        // $result = $this->fetchGeneric($data, $this->product);


        if($result == null){
            return $this->setResponse([
                "code" => 401,
                "title" => "No Contact Information Available",
            ]);
        }
        return $this->setResponse([
                    "code" => 200,
                    "title" => "Successfully retreive Product Contact",
                    "meta"        => [
                        "contact" => $result,
                        "count"=> $result -> count(),
                    ]
                ]);
    }
    


    public function setApproveRequestContact($data = [])
    {
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }
        if (!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }
        if (!isset($data['request_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "request_id is not set.",
            ]);
        }
            

        $result  = DB::table('request_contact')
        ->where('request_contact.user_id', '=',  $data['user_id'])
        ->where('request_contact.request_id', '=',  $data['request_id'])
        ->where('request_contact.product_id', '=',  $data['product_id'])
        ->where('request_contact.owner_approved', '=' , null)
        ->update([ 'request_contact.owner_approved' =>'1' ]);


        $exist  = DB::table('request_contact')
        ->where('request_contact.user_id', '=',  $data['user_id'])
        ->where('request_contact.request_id', '=',  $data['request_id'])
        ->where('request_contact.product_id', '=',  $data['product_id'])
        ->where('request_contact.owner_approved', '=' , '1')->get()->count();
        
        
        if($exist==1){
            return $this->setResponse([
                'code'  => 401,
                'title' => "Already Approved",
            ]);
        }
        if($result==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "No Contact Match",
                "meta"        => [
                    "exist" => $exist,
                ]
            ]);
        }

        $functions = new PushRepository();
        $var = $functions->push($data['user_id'],'Approving Contact' , '', $result , 'Chat');


        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully approve a contact id provided.",
            "meta"        => [
                "status" => $result,
            ]
        ]);
            
        
    }

}
