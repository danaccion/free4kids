<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\ApprovalModel;
use App\Data\Repositories\BaseRepository;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Data\Models\RequestContactModel;

class RequestContactRepository extends BaseRepository
{

    protected $approval;
    protected $reservation;

    public function __construct(RequestContactModel $requestContact) 
    {
        $this->requestContact = $requestContact;
    }

    public function getContactRequest($data = [])
    {
        $meta_index = "request_contact";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "request_contact";
            // $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "product_id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],   [
                    "target" => "request_id",
                    "operator" => "=",
                    "value" => $data['product_id'],
                ],
                [
                    "target" => "user_id",
                    "operator" => "=",
                    "value" =>  $data['user_id'],
                ]
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $approvedRequest  = DB::table('request_contact')
        ->where('request_contact.user_id', '=',  $data['user_id'])
        ->where('request_contact.request_id', '=',  $data['request_id'])
        ->where('request_contact.product_id', '=',  $data['product_id'])
        ->where('request_contact.owner_approved', '!=', null)
        ->get()->count();


        $result  = DB::table('request_contact')
        ->where('request_contact.user_id', '=',  $data['user_id'])
        ->where('request_contact.request_id', '=',  $data['request_id'])
        ->where('request_contact.product_id', '=',  $data['product_id'])
        ->where('request_contact.owner_approved', '!=', null)
        ->get();


        if($approvedRequest==1){
            return $this->setResponse([
                'code'  => 201,
                'title' => "Already Approved",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }


        $result = $this->fetchGeneric($data, $this->requestContact);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                 "count" => $this->requestContact::where('user_id','=',$data['user_id'])->
                 where('request_id','=',$data['request_id'])
                 ->
                 where('product_id','=',$data['product_id'])
                 ->get()->count()
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
            if (!isset($data['product_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "product_id is not set.",
                ]);
            }
           

            if (!isset($data['request_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "request_id is not set.",
                ]);
            }
           

            // if (!isset($data['description'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "description is not set.",
            //     ]);
            // }

            $approvedRequest  = DB::table('request_contact')
            ->where('request_contact.user_id', '=',  $data['user_id'])
            ->where('request_contact.request_id', '=',  $data['request_id'])
            ->where('request_contact.product_id', '=',  $data['product_id'])
            ->where('request_contact.owner_approved', '!=', null)
            ->get()->count();


       

            if($approvedRequest==1){
                return $this->setResponse([
                    'code'  => 201,
                    'title' => "Already Approved",
                ]);
            }

            $alreadyRequested  = DB::table('request_contact')
            ->where('request_contact.user_id', '=',  $data['user_id'])
            ->where('request_contact.request_id', '=',  $data['request_id'])
            ->where('request_contact.product_id', '=',  $data['product_id'])
            ->where('request_contact.owner_approved', '=', null)
            ->get()->count();
            
            if($alreadyRequested==1){
                return $this->setResponse([
                    'code'  => 202,
                    'title' => "Already Requested",
                ]);
            }

            $requestContact = $this->requestContact->init($this->requestContact->pullFillable($data));
            $requestContact->save($data);
        if (!$requestContact->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $requestContact->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully create a request.",
            "meta"        => [
                "requestContact" => $requestContact,
            ]
        ]);

    }
	
    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $requestContact = $this->requestContact->find($data['id']);
        if($requestContact==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$requestContact->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $requestContact->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a approval.",
            "meta"        => [
                "status" => $requestContact,
            ]
        ]);
            
        
    }
	
	
 	

}
