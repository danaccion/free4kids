<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\ClosedDealModel;
use App\Data\Repositories\BaseRepository;
use App\Approval;
use App\Data\Models\Points;
use App\Data\Models\Users;
use App\Data\Models\MessagesModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ReservationModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Models\ImageModel;
use App\Data\Models\TotalPointsModel;
use App\User;
use App\Image;
use App\Reports;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ClosedDealRepository extends BaseRepository
{

    protected $closeddeal;
    protected $reservation;
    protected $product;
    protected $points;
    protected $totalpoints;
    public function __construct(ClosedDealModel $closeddeal,
    ReservationModel $reservation, Product $product,Points $points,
    TotalPointsModel $totalpoints) 
    {
        $this->closeddeal = $closeddeal;
        $this->reservation = $reservation;
        $this->product = $product;
        $this->points = $points;
        $this->total_points = $totalpoints;
        
    }

    public function fetchClosedDeal($data = [])
    {
        $meta_index = "closeddeal";
        $parameters = [];
        $count = 0;

        $data['relations'] = ["Seller"];
        $data['wherehas'][] = [
            'relation' => 'Buyer',
            'target' => [
                [
                    'column' => "buyer_id",
                    "operator" => "=",
                    "value" => auth()->user()->id,
                ],
            ],
        ];


        $data['wherehas'][] = [
            'relation' => 'Seller',
            'target' => [
                [
                    'column' => "seller_id",
                    "operator" => "=",
                    "value" => $data['seller_id'],
                ],
            ],
        ];
        // if (isset($data['id']) &&
        //     is_numeric($data['id'])) {

        //     $meta_index = "closeddeal";
        //     $data['single'] = true;
        //     $data['where'] = [
        //         [
        //             "target" => "id",
        //             "operator" => "=",
        //             "value" => $data['id'],
        //         ],
        //     ];

        //     $parameters['id'] = $data['id'];

        // }

        // $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->closeddeal);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved closed deal",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }


    public function create($data = [])
    {
      
        if (!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }

        if (!isset($data['buyer_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "buyer_id is not set.",
            ]);
        }


        if (!isset($data['seller_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "seller_id is not set.",
            ]);
        }
        
        $product= $this->product->where([
            ['user_id', '=', ($data['buyer_id']) ? :auth()->user()->id]
    
        ])->get();

        ($closed = DB::table('reservation')
        ->where('reservation.product_id', '=', $data['product_id'])
        ->update([ 'reservation.deal' =>'Yes' ])) ? : "Cant Update closed";

        $prod = $this->product->find($data['product_id']);
      

        #if product_id does not exist
        if($prod == []){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Product_id not exist pick other product_id"
            ]);
        }

       #you cannot closed your own product
       if($prod->user_id == $data['buyer_id']){
               return $this->setResponse([
                   'code'  => 500,
                   'title' => "productowner cant closed Deal only buyers",
               ]);
             }

        #identifies exist on closed deal
        $data['user_id'] = $data['buyer_id']; 
        $prod= ClosedDealModel::where([

                ['product_id', '=', $data['product_id']],
        
                ['buyer_id', '=', $data['buyer_id']]
        
        ])->get();

           
        if($prod->count() > 0)   
        return $this->setResponse([
                    'code'  => 404,
                    'title' => "Already closed deal",
        ]);

  
                # d8888b.  .d88b.  d888888b d8b   db d888888b .d8888.
                # 88  `8D .8P  88.   `88'   888o  88 `~~88~~' 88'  YP
                # 88oodD' 88  d'88    88    88V8o 88    88    `8bo.  
                # 88~~~   88 d' 88    88    88 V8o88    88      `Y8b.
                # 88      `88  d8'   .88.   88  V888    88    db   8D
                # 88       `Y88P'  Y888888P VP   V8P    YP    `8888Y'

                # SIMPLE POINTS INCREMENTATION
                   
        #total_points    
        $points = $this->points->init($this->points->pullFillable($data));
        $points->save($data);

        $exist_user= DB::table('total_points')->where([
        ['user_id', '=', $data['seller_id']]
        ])->get();
                #if user exist true update value points field
                if($exist_user->count() > 0)   
                {
                    #update total_point
                    if($tp = DB::table('total_points')
                    ->where('user_id', '=', $data['seller_id'])
                    ->increment('points', 1) #in every current point it adds
                    )
                    {
                        // echo json_encode("success add point"); #indicators
                    }
                }
                #if user does not exist create user +1 point
                else{
                    #insert user

                    $data['points'] = 0;
                    $data['bonus'] = 0;
                    $data['user_id'] = $data['seller_id']; #user_id = seller_id
                    $tp = $this->total_points->init($this->total_points->pullFillable($data));
                    $tp->save($data);
                }

        if (!$points->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error Points.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $points->errors(),
                ],
            ]);
        }
             

        $closeddeal = $this->closeddeal->init($this->closeddeal->pullFillable($data));
        $closeddeal->save($data);
        if (!$closeddeal->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error Closed Deal.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $closeddeal->errors(),
                ],
            ]);
        }

                     
                    #  d8888b.  .d88b.  d8b   db db    db .d8888.
                    #  88  `8D .8P  88. 888o  88 88    88 88'  YP
                    #  88oooY' 88  d'88 88V8o 88 88    88 `8bo.  
                    #  88~~~b. 88 d' 88 88 V8o88 88    88   `Y8b.
                    #  88   8D `88  d8' 88  V888 88b  d88 db   8D
                    #  Y8888P'  `Y88P'  VP   V8P ~Y8888P' `8888Y'

                    # BONUS INSERTION ALGORITHM

        
        $point= DB::table('points')->where([

            ['seller_id', '=', $data['seller_id']],
    
            ['marker', '=', NULL]
    
        ])->get();
                #if user has 5 closeddeals
                if($point->count() % 5 == 0)   
                {
                 if($point->count() == 0){
                    echo json_encode("if table is empty".$point->count()); #indicators
                 }else{
                
                    #TOTAL POINTS BONUS MERGE ON POINTS
                    #SPECIFIC BONUS ON TOTALPOINTS TRACK
                    if(DB::table('total_points')
                    ->where('user_id', '=', $data['seller_id'])
                    ->increment('bonus', 1)){
                       

                             #B0NUS UPDATE
                             if(DB::table('points')->where([

                                ['seller_id', '=', $data['seller_id']],
                        
                                ['marker', '=', NULL]               
                            ])
                            ->update([ 'marker' =>'done' ])){
                                #stored procedure for notif
                                if($stored = DB::statement('call raw_data(?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?)',[$data['product_id'], NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Bonus', $data['seller_id'], NULL, NULL, 'this is a bonus', NULL, NULL,NULL,"No" ])){
                                    echo json_encode("stored procedure success & add bonus"); #indicators
                                }else{
                                    echo json_encode("failed to perform #storedprocedure ".$stored);
                                }

                            }else{
                               
                                return $this->setResponse([
                                    "code"       => 500,
                                    "title"      => "total points on done update",
                                ]); 
                               
                            }
                    }else{
                        return $this->setResponse([
                            "code"       => 500,
                            "title"      => $ClosedProducts,
                        ]);
                    }
                    #update total_point
                     }
                }else{
                    $tp = DB::table('total_points')
                    ->where('user_id', '=', $data['seller_id'])
                    ->increment('points', 1);
                }


             
                


     $functions = new PushRepository();
    // $var = $functions->push($data['receiver_id'],$data['text'],$name[0], $messages, $view    );
     $name = Users::where('id',$data['user_id'])->pluck('name');
     $var = $functions->push($data['user_id'],'Successfully Closed Deal' ,$name[0],$closeddeal ,'Closed_deal');
     $name = Users::where('id',$data['buyer_id'])->pluck('name');
     $var = $functions->push($data['buyer_id'],'Successfully Closed Deal',$name[0] ,$closeddeal , 'Closed_deal');
        if(!$var)
        {   return $this->setResponse([
            "code"       => 404,
            "title"      => "Failed to Push notification.",
        ]);
        }
        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully create closeddeal.",
            "meta"        => [
                "status" => $closeddeal,
            ]
        ]);
        
    }

    
    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['reserved_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reserved_id is not set.",
            ]);
        }
        
        if (!isset($data['approved'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approved is not set.",
            ]);
        }

        if (!isset($data['rejected'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "rejected is not set.",
            ]);
        }
        $closeddeal = $this->closeddeal->find($data['id']);
        if($closeddeal==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $closeddeal->save($data);
        if (!$closeddeal->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $closeddeal->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a closeddeal.",
            "meta"        => [
                "status" => $closeddeal,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $closeddeal = $this->closeddeal->find($data['id']);
        if($closeddeal==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$closeddeal->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $closeddeal->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a closeddeal.",
            "meta"        => [
                "status" => $closeddeal,
            ]
        ]);
            
        
    }


}
