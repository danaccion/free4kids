<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\ActivitiesModel;
use App\Data\Repositories\BaseRepository;
use App\Approval;
use App\Data\Models\Users;
use App\Data\Models\MessagesModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ReservationModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Models\ImageModel;
use App\User;
use App\Image;
use App\Reports;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ActivitiesRepository extends BaseRepository
{

    protected $activities;
    protected $reservation;

    public function __construct(ActivitiesModel $activities,
    ReservationModel $reservation) 
    {
        $this->activities = $activities;
        $this->reservation = $reservation;
    }

    public function fetchActivities($data = [])
    {
        $meta_index = "activities";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "activities";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->activities);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
      
        if (!isset($data['notif_type_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "notif_type_id is not set.",
            ]);
        }

        if (!isset($data['action'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "action is not set.",
            ]);
        }

        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }


        if (!isset($data['description'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "description is not set.",
            ]);
        }

        $activities = $this->activities->init($this->activities->pullFillable($data));
        $activities->save($data);
        if (!$activities->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $activities->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully create activities.",
            "meta"        => [
                "status" => $activities,
            ]
        ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['reserved_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reserved_id is not set.",
            ]);
        }
        
        if (!isset($data['approved'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approved is not set.",
            ]);
        }

        if (!isset($data['rejected'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "rejected is not set.",
            ]);
        }
        $activities = $this->activities->find($data['id']);
        if($activities==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $activities->save($data);
        if (!$activities->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $activities->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a activities.",
            "meta"        => [
                "status" => $activities,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $activities = $this->activities->find($data['id']);
        if($activities==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$activities->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $activities->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a activities.",
            "meta"        => [
                "status" => $activities,
            ]
        ]);
            
        
    }


}
