<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\ContactModel;
use App\Data\Repositories\BaseRepository;
use App\Contact;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ContactRepository extends BaseRepository
{

    protected $Contact;

    public function __construct(ContactModel $contact
   ) 
    {
        $this->contact = $contact;
    }

    public function create($data = [])
    {
        // data validation
            if (!isset($data['user_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "user_id is not set.",
                ]);
            }

            if (!isset($data['number'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "number is not set.",
                ]);
            }
         
            $contact = $this->contact->init($this->contact->pullFillable($data));
        if (!$contact->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $contact->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully create a contact.",
            "meta"        => [
                "status" => $contact,
            ]
        ]);

    }

}
