<?php

namespace App\Data\Repositories;

use App\Data\Models\SaveModel;
use App\Data\Models\DelsaveModel;
use App\Data\Repositories\BaseRepository;
use App\Save;
use App\Data\Models\Product;
use App\Data\Models\followModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use DateTime;
use DB;
class saveRepository extends BaseRepository
{

    protected $save;
    protected $del;
    protected $follow;
    public function __construct(SaveModel $save,DelsaveModel $del, Product $product, followModel $follow) 
    {
        $this->save = $save;
        $this->del = $del;
        $this->product = $product;
        $this->follow = $follow;
    }

    public function fetchsave($data = [])
    {
        $meta_index = "saved_products";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "save";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->save);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Saved Products are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        $prod = $this->product->find($data['product_id']);
          
        $data['user_id']=$prod ->user_id;
       
        if($prod->user_id==auth()->user()->id){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Owner of product cannot use save",
            ]);
          }

        if (!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }   
        $data['follower'] = auth()->user()->id;

        
        $countsave = DB::table('saved_products')
        ->where('user_id', '=',$data['follower'] )
        ->where('product_id', '=',$data['product_id'] )
        ->where('deleted_at', '=', null);
      
        if( $countsave->get()->count() > 0)   
        return $this->setResponse([
                'code'  => 500,
                'title' => "Already saved product",
            ]);


            
            $data['follower'] = auth()->user()->id;
            $res = DB::table('follow')
            ->where('follower', '=', auth()->user()->id )
            ->where('user_id', '=',$prod->user_id);
    
             $data['status']=1;
            $datetime = new DateTime();
            $datetime->modify('+2 day');
            $data['expiry']=$datetime->format('Y-m-d H:i:s');
            $data['user_id']=  $data['follower'];
            $save = $this->save->init($this->save->pullFillable($data));
            $save->save($data);



            if (!$save->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $save->errors(),
                    ],
                ]);
            }
       
           
            // if($res->count() < 2)   {
            //     $data['user_id'] = $prod->user_id;
            //     $data['status'] ="1"; 
            //     $follow = $this->follow->init($this->follow->pullFillable($data));
            //     $follow->save($data);
            //      }
          

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully follow and save.",
                "parameters" => $save,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['reserved_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reserved_id is not set.",
            ]);
        }
        
        if (!isset($data['approved'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approved is not set.",
            ]);
        }

        if (!isset($data['rejected'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "rejected is not set.",
            ]);
        }
        $save = $this->save->find($data['id']);
        if($save==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $save->save($data);
        if (!$save->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $save->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a save.",
            "meta"        => [
                "status" => $save,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }

        $del = $this->save->find($data['product_id']);
        if($del==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$del->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $del->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a save.",
            "meta"        => [
                "status" => $del,
            ]
        ]);
            
        
    }


}
