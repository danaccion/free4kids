<?php

namespace App\Data\Repositories;

use App\Data\Models\BonusModel;
use App\Data\Repositories\BaseRepository;
use App\bonus;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class BonusRepository extends BaseRepository
{

    protected $bonus;

    public function __construct(BonusModel $bonus) 
    {
        $this->bonus = $bonus;
    }

    public function fetchbonus($data = [])
    {
        $meta_index = "bonus";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "bonus";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->bonus);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No bonus found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved bonus",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        
    //    $data['user_id'] = auth()->user()->id;
            if (!isset($data['user_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "user_id is not set.",
                ]);
            }
        
            if (!isset($data['bonus'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "bonus is not set.",
                ]);
            }

            // if (!isset($data['product_id'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "bonus is not set.",
            //     ]);
            // }
    
            $bonus = $this->bonus->init($this->bonus->pullFillable($data));
            $bonus->save($data);

            if (!$bonus->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $bonus->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully create a bonus.",
                "parameters" => $bonus,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }
   
        $bonus = $this->bonus->find($data['id']);
        
        $bonus->save($data);
        if (!$bonus->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $bonus->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a bonus.",
            "meta"        => [
                "status" => $bonus,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $bonus = $this->bonus->find($data['id']);
        if($bonus==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$bonus->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $bonus->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a bonus.",
            "meta"        => [
                "status" => $bonus,
            ]
        ]);
            
        
    }


}
