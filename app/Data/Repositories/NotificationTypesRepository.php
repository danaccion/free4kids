<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\NotificationTypesModel;
use App\Data\Repositories\BaseRepository;
use App\Approval;
use App\Data\Models\Users;
use App\Data\Models\MessagesModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ReservationModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Models\ImageModel;
use App\User;
use App\Image;
use App\Reports;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class NotificationTypesRepository extends BaseRepository
{

    protected $notification;
    protected $reservation;

    public function __construct(NotificationTypesModel $notification,
    ReservationModel $reservation) 
    {
        $this->notification = $notification;
        $this->reservation = $reservation;
    }

    public function fetchnotificationtypes($data = [])
    {
        $meta_index = "notificationtypes";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "notificationtypes";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->notification);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
      
        if (!isset($data['name'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "name is not set.",
            ]);
        }

        if (!isset($data['description'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "description is not set.",
            ]);
        }



  

        $notification = $this->notification->init($this->notification->pullFillable($data));
        $notification->save($data);
        if (!$notification->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $notification->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully create notification type.",
            "meta"        => [
                "status" => $notification,
            ]
        ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['reserved_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reserved_id is not set.",
            ]);
        }
        
        if (!isset($data['approved'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approved is not set.",
            ]);
        }

        if (!isset($data['rejected'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "rejected is not set.",
            ]);
        }
        $notification = $this->notification->find($data['id']);
        if($notification==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $notification->save($data);
        if (!$notification->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $notification->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a notification.",
            "meta"        => [
                "status" => $notification,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $notification = $this->notification->find($data['id']);
        if($notification==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$notification->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $notification->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a notification.",
            "meta"        => [
                "status" => $notification,
            ]
        ]);
            
        
    }


}
