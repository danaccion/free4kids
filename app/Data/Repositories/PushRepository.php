<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\ApprovalModel;
use App\Data\Repositories\BaseRepository;
use App\Approval;
use App\Data\Models\Users;
use App\Data\Models\MessagesModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ReservationModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Models\ImageModel;
use App\User;
use App\Image;
use App\Reports;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class PushRepository extends BaseRepository
{

    protected $approval;
    protected $reservation;

    public function __construct() 
    {
    }   

    public function push($user_id, $body , $title , $result, $view)
    {
		$token = Users::where('id',$user_id)->pluck('notification_token');
        $payload = array(
			'to' => 'ExponentPushToken['.$token[0].']',
			'sound' => 'default',
			'body' => $body,
			'title'=> $title,
			'data' => /*(object) array('data' => $result)*/ $result,
			
		);
		//(object) array('data' => 'foo')
	
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://exp.host/--/api/v2/push/send",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($payload),
			CURLOPT_HTTPHEADER => array(
				"Accept: application/json",
				"Accept-Encoding: gzip, deflate",
				"Content-Type: application/json",
				"cache-control: no-cache",
				"host: exp.host"
			),
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			/*if ($err) {
			echo "cURL Error #:" . $err;
			} else {
			echo $response;
			}*/
    }
  

}
