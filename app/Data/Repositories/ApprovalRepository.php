<?php

namespace App\Data\Repositories;
use DB;
use App\Data\Models\ApprovalModel;
use App\Data\Repositories\BaseRepository;
use App\Approval;
use App\Data\Models\Users;
use App\Data\Models\MessagesModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ReservationModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Models\ImageModel;
use App\User;
use App\Image;
use App\Reports;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ApprovalRepository extends BaseRepository
{

    protected $messages;
    protected $approval;
    protected $reservation;

    public function __construct(ApprovalModel $approval,
    ReservationModel $reservation) 
    {
        $this->approval = $approval;
        $this->reservation = $reservation;
    }

    public function fetchapproval($data = [])
    {
        $meta_index = "approval";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "approval";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->approval);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }


    public function create($data = [])
    {
        // data validation
            if (!isset($data['reserved_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "reserved_id is not set.",
                ]);
            }
            // if (!isset($data['lack_of_approved'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "reserved_id is not set.",
            //     ]);
            // }
            // if (!isset($data['approved'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "approved is not set.",
            //     ]);
            // }
            // $approval->save($data);
            $approval = $this->approval->init($this->approval->pullFillable($data));
            $approval->save($data);
        if (!$approval->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $approval->errors(),
                ],
            ]);
        }

        $functions = new PushRepository();
        $var = $functions->push($data['user_id'],'You have new notification' , '', $approval, 'Item Has been Approved!');
           if(!$var)
           {   return $this->setResponse([
               "code"       => 404,
               "title"      => "Failed to Push notification.",
           ]);
           }

        

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully create a approval.",
            "meta"        => [
                "status" => $approval,
            ]
        ]);

    }
	
    public function reservationstatus($data = [])
    {
        // data validation
        
          
            // if (!isset($data['reserved_id'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "reserved_id is not set.",
            //     ]);
            // }
            // if (!isset($data['lack_of_approved'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "reserved_id is not set.",
            //     ]);
            // }
            // if (!isset($data['rejected'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "rejected is not set.",
            //     ]);
            // }
            // if (!isset($data['approved'])) {
            //     return $this->setResponse([
            //         'code'  => 500,
            //         'title' => "approved is not set.",
            //     ]);
            // }

       

        //     $approval = $this->approval->init($this->approval->pullFillable($data));
        //     $approval->save($data);


        // $meta_index = "users";

       
        // $userObj = Users::all();
        // $prodObj = ReservationModel::all()->where('id',$data['reserved_id']);
        // $result = [];
        // foreach ($userObj as $key => $value) {
        //             $user_id = $value->id;
                   
                   
        //             foreach ($prodObj as $key => $value2) {
        //               if($value2->user_id == $user_id)
        //               {
        //                 array_push($result, $value);
        //                 $prod_id = $value2->id;
        //                // array_push($result, "reservation");
        //               //  array_push($result, $value2);
        //               }
        //             }
        //     }
            
            
        //     $a["Users"] = $result;
        //     $a["Request"][] = $approval;
        
        $reservation = $this->reservation->find($data['id']);
        
        $reservation->save($data);
        if (!$reservation->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $reservation->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a reservation.",
            "meta"        => [
                "status" => $reservation,
            ]
        ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['reserved_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reserved_id is not set.",
            ]);
        }
        
        if (!isset($data['approved'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approved is not set.",
            ]);
        }

        if (!isset($data['rejected'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "rejected is not set.",
            ]);
        }
        $approval = $this->approval->find($data['id']);
        if($approval==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $approval->save($data);
        if (!$approval->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $approval->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a approval.",
            "meta"        => [
                "status" => $approval,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $approval = $this->approval->find($data['id']);
        if($approval==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$approval->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $approval->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a approval.",
            "meta"        => [
                "status" => $approval,
            ]
        ]);
            
        
    }
	
	
 	

}
