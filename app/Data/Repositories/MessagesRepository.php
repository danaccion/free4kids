<?php

namespace App\Data\Repositories;
use App\Data\Models\Users;
use App\Data\Models\MessagesModel;
use App\Data\Models\MessagesDeleteById;
use App\Data\Repositories\BaseRepository;
use App\Data\Models\ImageModel;
use App\Message;
//use App\Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Db;
use Intervention\Image\ImageManagerStatic as Image;
require '../vendor/autoload.php';
class MessagesRepository extends BaseRepository
{

    protected $messages;
    protected $msgbyid;
    protected $image;

    public function __construct(MessagesModel $messages,MessagesDeleteById $msgbyid) 
    {
        $this->messages = $messages;
        $this->msgbyid = $msgbyid;
       // $this->image = $image;
    }

    public function fetchMessages($data = [])
    {
        $meta_index = "messages";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "messages";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->messages);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->messagess->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }   

    public function create($data = [])
    {
        // data validation
        
        // if (!isset($data['chat_id'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "chat id is not set.",
        //     ]);
        // }
        // if (!isset($data['user_id'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "user id is not set.",
        //     ]);
        // }

        // if (!isset($data['location_lat'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "location_lat is not set.",
        //     ]);
        // }
        
        // if (!isset($data['location_long'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "location_long is not set.",
        //     ]);
        // }

        if (!isset($data['_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "_id is not set.",
            ]);
        }

        if (!isset($data['sender_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "sender is not set.",
            ]);
        }
        
        if (!isset($data['receiver_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "receiver_id is not set.",
            ]);
        }

        if (!isset($data['type'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "type is not set.",
            ]);
        }
        // if (!isset($data['messages'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "messages is not set.",
        //     ]);
        // }
        // if (!isset($data['approval_id'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "approval_id is not set.",
        //     ]);
        // }
        $data['user_id'] = $data['sender_id'];
         if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user id is not set.",
            ]);
        }
        if (!isset($data['image'])) {
		}else{
			   $image_url=null;
            if(isset($request['image'])){
                request()->validate([

                    'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',

                ]);
                $imageName = time().'.'.request()->image->getClientOriginalExtension();
                define('UPLOAD_DIR', 'storage/images/');
                $resize = Image::make($request['image'])->resize(400,400)->encode('jpg');   
                $file =  $resize->save(UPLOAD_DIR.$imageName,60);
                $url= asset(UPLOAD_DIR.$imageName);
                $image_url = $url;  
            
            }
			
			if( !$image->save($data))
        {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $image->errors(),
                ],
            ]);
        }
			
			
			        $imageObj = $image::where('user_id',$data['user_id']) 
        ->orderBy('id', 'desc')
        ->take(1)
        ->get('id');

        foreach ($imageObj as $key => $value) {
            $image_id = $value->id;
        }
        $data['image_id'] = $image_id;
            if (!isset($data['image_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "image id is not set.",
                ]);
            }
		}
        





       
            $messages = $this->messages->init($this->messages->pullFillable($data));
            $messages->save($data);

            if (!$messages->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $messages->errors(),
                    ],
                ]);
            }

    $name = Users::where('id',$data['user_id'])->pluck('name');
    $functions = new PushRepository();
    $view = 'ApproveChat';
    if(strcasecmp($data['type'], 'buyer') == 0)
    {
        $view = 'ReservationChat';
    }
    $var = $functions->push($data['receiver_id'],$data['text'],$name[0], $messages, $view    );

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully create message.",
                "parameters" => $messages,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['chat_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "chat_id is not set.",
            ]);
        }
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user id is not set.",
            ]);
        }
        
        if (!isset($data['location_lat'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "location_lat is not set.",
            ]);
        }
        
        if (!isset($data['location_long'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "location_long is not set.",
            ]);
        }

        if (!isset($data['sender_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "sender_id is not set.",
            ]);
        }
        
        if (!isset($data['receiver_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "receiver_id is not set.",
            ]);
        }
        if (!isset($data['messages'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "messages is not set.",
            ]);
        }
        if (!isset($data['approval_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approval_id is not set.",
            ]);
        }
        if (!isset($data['image'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "image is not set.",
            ]);
        }else{
           /* $data['image_url']=null;      
                request()->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',
        
                ]);
                $imageName = time().'.'.request()->image->getClientOriginalExtension();
                define('UPLOAD_DIR', 'storage/images/');
                $file =  request()->image->move(UPLOAD_DIR,$imageName);
                $url= asset($file);
                $data['image_url'] = $url;   */
			
			$image_url=null;
            if(isset($request['image'])){
                request()->validate([

                    'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',

                ]);
                $imageName = time().'.'.request()->image->getClientOriginalExtension();
                define('UPLOAD_DIR', 'storage/images/');
                $resize = Image::make($request['image'])->resize(400,400)->encode('jpg');   
                $file =  $resize->save(UPLOAD_DIR.$imageName,60);
                $url= asset(UPLOAD_DIR.$imageName);
                $image_url = $url;  
            
            }
        }

        $messages = $this->messages->find($data['id']);
        if($messages==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $messages->save($data);
        if (!$messages->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $messages->errors(),
                ],
            ]);
        }


//$token     
        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a messages.",
            "meta"        => [
                "status" => $messages,
            ]
        ]);
    }

    public function updateSeen($data = [])
    {
       
        if (!isset($data['sender_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "sender_id is not set.",
            ]);
        }
        
        if (!isset($data['receiver_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reciever_id is not set.",
            ]);
        }



        $message = Message::where('sender_id',$data['sender_id'])
                                 ->where('reciever_id',$data['reciever_id'])
                                 ->update(['seen'=>1]);
                                    
        if($message==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }
       

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a messages.",
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $messages = $this->messages->find($data['id']);
        if($messages==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "messages not found.",
            ]);
        }
        
        if (!$messages->delete()) { 
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $messages->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a messages.",
            "meta"        => [
                "status" => $messages,
            ]
        ]);
            
        
    }

    public function deleteMessagebyid($data = [])
    {
       
        if (!isset($data['sender_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "sender is not set.",
            ]);
        }
        
        if (!isset($data['receiver_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "receiver_id is not set.",
            ]);
        }

        if (!isset($data['date'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "date is not set.",
            ]);
        }
        $msgbyid = $this->msgbyid->init($this->msgbyid->pullFillable($data));
        $msgbyid->save($data);
            if (!$msgbyid->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $msgbyid->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully delete request message.",
                "parameters" => $msgbyid,
            ]);
        
    }


}
