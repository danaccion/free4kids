<?php

namespace App\Data\Repositories;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Data\Models\Users;
use App\Data\Models\RawData_Model;
use App\Data\Models\MergeNotifModel;
use App\Data\Models\Total_Model;
use App\Data\Models\ApprovalModel;
use App\Data\Models\followModel;
use App\Data\Models\MessagesModel;
use App\Data\Models\Location;
use App\Data\Models\Product;
use App\Data\Models\ProductUserid;
use App\Data\Models\ReservationModel;
use App\Data\Models\ProductCategory;
use App\Data\Models\ReportsModel;
use App\Data\Models\ReviewsModel;
use App\Data\Models\saveModel;
use App\Data\Models\ContactModel;
use App\Data\Repositories\BaseRepository;
use App\Data\Models\ImageModel;
use App\Data\Models\TotalPointsModel;
use App\User;
use App\Contact;
use App\Data\Models\ProcessRawModel;
use App\Products;
use App\Image;
use App\Reports;
use App\Reservations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UsersRepository extends BaseRepository
{

    protected $users;
    protected $image;
    protected $reservation;
    protected $products;
    protected $category;
    protected $saveModel;
    protected $puid;
    protected $follow;
    protected $messages;
    protected $reviews;
    protected $mergenotif;
    protected $total_order;
    protected $raw_data;
    protected $contact;
    public function __construct(
        Users $users , Location $location,
        ReportsModel $reports,
        Product $products,
        ProductUserid $puid,
        ImageModel $image,
        ReservationModel $reservation,
        saveModel $saveModel,
        ProductCategory $category,
        followModel $follow,
        MessagesModel $messages,
        ReviewsModel $reviews,
        MergeNotifModel $mergenotif,
        Total_Model $total_order,
        RawData_Model $raw_data,
        ProcessRawModel $raw,
        ContactModel $contact
    ) {
        $this->users = $users;
        $this->location = $location;
        $this->reports = $reports;
        $this->products = $products;
        $this->image = $image;
        $this->reservation = $reservation;
        $this->saveModel = $saveModel;
        $this->category = $category;
        $this->puid = $puid;
        $this->follow = $follow;
        $this->messages = $messages;
        $this->mergenotif = $mergenotif;
        $this->total_order = $total_order;
        $this->raw_data = $raw_data;
        $this->raw = $raw;
        $this->contact = $contact;
    }

    public function fetchUser($data = [])
    {
        $meta_index = "users";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "users";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->users);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function location($data = [])
    {
        $meta_index = "users";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["location"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {

            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
            ];

            $parameters['user_id'] = $data['user_id'];

        }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->users);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function reports($data = [])
    {
        $meta_index = "users";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["reports"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {

            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],

            ];

            $parameters['user_id'] = $data['user_id'];

        }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->users);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }




    public function products($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['relations'] = ["image","Users","delivery","location","reservation_status1","product_reports"];

        if (isset($data['product_id'])) {
        $data['where'] = [
            [
                "target" => "id",
                "operator" => "=",
                "value" => $data['product_id'],
            ],
            [
                "target" => "status",
                "operator" => "=",
                "value" => "active",
            ],
            [
                "target" => "reservationexclude",
                "operator" => "=",
                "value" => NULL,
            ],
            [
                "target" => "closed",
                "operator" => "=",
                "value" => NULL,
            ],

        ];
    }else{
        $data['where'] = [
            [
                "target" => "status",
                "operator" => "=",
                "value" => "active",
            ],
            [
                "target" => "reservationexclude",
                "operator" => "=",
                "value" => NULL,
            ],
            [
                "target" => "closed",
                "operator" => "=",
                "value" => NULL,
            ],

        ];
    }

        if (isset($data['category_id'])) {
            $data['wherehas'][] = [
                'relation' => 'productstocategory',
                'target' => [
                    [
                        'column' => 'category_id',
                        'value' => $data['category_id'],
                    ],
                ],
            ];
        }
        if (isset($data['user_id'])) {
            $data['wherehas'][] = [
                'relation' => 'products',
                'target' => [
                    [
                        'column' => 'user_id',
                        'value' => $data['user_id'],
                    ],
                ],
            ];
        }




        $result = $this->fetchGeneric($data, $this->products);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                "total_product" => $this->products->count()
            ],
            "parameters" => $parameters,
        ]);
    }


    public function total_order($data = [])
    {
        $meta_index = "total_orders";
        $parameters = [];
        $count = 0;

        $data['single'] = true;
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {

            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],

            ];

            $parameters['user_id'] = $data['user_id'];

        }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->total_order);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }


    public function searchProducts($data = [])
    {
        if (!isset($data['query'])) {
            return $this->setResponse([
                "code" => 500,
                "title" => "Query is not set",
                "parameters" => $data,
            ]);
        }

        $meta_index = "product";
        $parameters = [
            "query" => $data['query'],
        ];
        $result = $this->products;
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['relations'] = ["image","Users","delivery","location"];


        if (isset($data['category_id'])) {
            $data['wherehas'][] = [
                'relation' => 'productstocategory',
                'target' => [
                    [
                        'column' => 'category_id',
                        'value' => $data['category_id'],
                    ],
                ],
            ];
        }
        if (isset($data['user_id'])) {
            $data['wherehas'][] = [
                'relation' => 'products',
                'target' => [
                    [
                        'column' => 'user_id',
                        'value' => $data['user_id'],
                    ],
                ],
            ];
        }

        if (isset($data['target'])) {
            foreach ((array) $data['target'] as $index => $column) {
                if (str_contains($column, 'name')) {
                    $data['target'][] = 'name';
                    unset($data['target'][$index]);
                }
            }

        }

        $result = $this->genericSearch($data, $result)->get()->all();

        if ($result == null) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No products are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count_data['search'] = true;
        // $result = $this->fetchGeneric($data, $this->products);

        // if (!$result) {
        //     return $this->setResponse([
        //         'code' => 404,
        //         'title' => "No agents are found",
        //         "meta" => [
        //             $meta_index => $result,
        //         ],
        //         "parameters" => $parameters,
        //     ]);
        // }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
            ],
            "parameters" => $parameters,
        ]);
    }




	  public function saved_products($data = [])
    {
        $meta_index = "product";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['relations'] = ["products","users"];
        // $data['where'] = [
        //     [
        //         "target" => "user_id",
        //         "operator" => "=",
        //         "value" =>  auth()->user()->id,
        //     ],
        // ];
        // $data['wherehas'][] = [
        //     'relation' => 'users',
        //     'target' => [
        //         [
        //             'column' => 'id',
        //             'value' => ,
        //         ],
        //     ],
        // ];


        $result = $this->fetchGeneric($data, $this->saveModel);
		// $reservation = $this->image->where("product_id",'=',$this->products->product_id)->first();

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function set_category_id($data = [])
    {
        $meta_index = "category";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['relations'] = ["products"];
        if (isset($data['category_id'])) {
            $data['wherehas'][] = [
                'relation' => 'product_category',
                'target' => [
                    [
                        'column' => 'id',
                        'value' => $data['category_id'],
                    ],
                ],
            ];
        }
        if (isset($data['user_id'])) {
            $data['wherehas'][] = [
                'relation' => 'products',
                'target' => [
                    [
                        'column' => 'user_id',
                        'value' => $data['user_id'],
                    ],
                ],
            ];
        }
        // $meta_index = "users";
        // $parameters = [];
        // $count = 0;
        // $data['relations'] = ["products"];
        // $data['sort'] = 'created_at';
        // $data['order'] = 'desc';
        // if (isset($data['user_id']) &&
        //     is_numeric($data['user_id'])) {
        //     $data['single'] = true;
        //     $data['where'] = [
        //         [
        //             "target" => "id",
        //             "operator" => "=",
        //             "value" => $data['user_id'],
        //         ],
        //     ];
        //     $parameters['user_id'] = $data['user_id'];
        // }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->category);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function product_category($data = [])
    {
        // $meta_index = "users";
        // $pcatObj = ProductCategory::all();
        // $prodObj = Product::all();
        // $userObj = Users::all()->where('id',$data['user_id']);
        // $result = [];
        // foreach ($userObj as $key => $value) {
        //             $user_id = $value->id;
        //             array_push($result, "users");
        //             array_push($result, $value);
        //             foreach ($prodObj as $key => $value2) {
        //               if($value2->user_id == $user_id)
        //               {
        //                 $prod_id = $value2->id;
        //                 array_push($result, "product");
        //                 array_push($result, $value2);
        //                 foreach ($pcatObj as $key => $value3) {
        //                     if($value3->id== $prod_id)
        //                     {
        //                       array_push($result, "product category");
        //                       array_push($result, $value3);
        //                     }
        //                   }
        //               }
        //             }
        //     }
         $meta_index = "category";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["products"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {
            $data['single'] = false;
            $data['wherehas'][] = [
                'relation' => 'reservation',
                'target' => [
                    [
                        'column' => 'reservation.product_id',
                        'operator' => '!=',
                        'value' =>'null',
                    ]
                             ]
                    ];
            $parameters['user_id'] = $data['user_id'];
        }


        $result = $this->fetchGeneric($data, $this->category);



        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Users are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved users->product->category",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    }

    public function all_product_category($data = [])
    {

        $meta_index = "category";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["products"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {
            $data['single'] = false;
            $data['wherehas'][] = [
                'relation' => 'reservation',
                'target' => [
                    [
                        'column' => 'reservation.product_id',
                        'operator' => '!=',
                        'value' =>'null',
                    ]
                ]
                    ];
            $parameters['user_id'] = $data['user_id'];
        }


        $result = $this->fetchGeneric($data, $this->category);

        // $meta_index = "users";
        // $presObj = ReservationModel::all();
        // $prodObj = Product::all();
        // $userObj = Users::all()->where('id',$data['user_id']);
        // $result = [];
        // foreach ($userObj as $key => $value) {
        //             $user_id = $value->id;
        //             array_push($result, "users");
        //             array_push($result, $value);
        //             foreach ($prodObj as $key => $value2) {
        //               if($value2->user_id == $user_id)
        //               {
        //                 $prod_id = $value2->id;
        //                 array_push($result, "product");
        //                 array_push($result, $value2);
        //                 foreach ($presObj as $key => $value3) {
        //                     if($value3->product_id == $prod_id)
        //                     {
        //                       array_push($result, "product reservation");
        //                       array_push($result, $value3);
        //                     }
        //                   }
        //               }
        //             }
        //     }


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Users are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved product - > reservation",
            "description" => "parameter either with user_id | post method",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    }



    public function reviews($data = [])
    {
        $meta_index = "users";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["reviews"];
        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {

            $meta_index = "users";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
            ];

            $parameters['user_id'] = $data['user_id'];

        }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->users);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved profile reviews",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }
    public function activity($data = [])
    {
		$total_points = [];
        $meta_index = "activity";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";

        if (isset($data['user_id']) &&
            is_numeric($data['user_id'])) {

            $meta_index = "activity_list";
            $data['single'] = false;
            $data['where'] = [
                [
                    "target" => "user_id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
                [
                    "target" => "notif",
                    "operator" => "!=",
                    "value" => "Yes",
                ]
            ];

            $parameters['user_id'] = $data['user_id'];

        }

        $count_data = $data;

        $result = $this->fetchGeneric($data, $this->raw_data);
        $total_points['total_points'] = 0;

        $functions = new PushRepository();
        $var = $functions->push($data['user_id'],'Aktivity' , '', $result , 'Activity');

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,

            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));
        $total = TotalPointsModel::where('user_id',$result[0]['user_id'])->pluck('points');

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved activity",
            "meta" => [
                $meta_index => $result,
                "total_points" => $total[0],
            ],

            "parameters" => $parameters,
        ]);
    }

    public function notification($data = [])
    {
		$total_points = [];
        $meta_index = "notification";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";

       $data['relations'] = ["Users"];

       $data['where'] = [
        [
            "target" => "user_id",
            "operator" => "=",
            "value" => $data['user_id'],
        ],
    ];

       $result = $this->fetchGeneric($data, $this->raw);


$functions = new PushRepository();
$var = $functions->push($data['user_id'],'You have new notification' , '', $result, 'Activity');
//$token
if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],

                "parameters" => $parameters,

            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));
        // $total = RawData_Model::where('user_id',$result[0]['follower_id'])
        // ->where('type','add_product')->orderByDesc('id')
        // ->get();

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved notification_list",
            "meta" => [
                $meta_index => $result,
                // "Followed User Added Products " =>  $total  ,
              //  "User_info" => Users::where('id',$data['user_id'])->get(),
            ],

            "parameters" => $parameters,
        ]);
    }

    public function reservation($data = [])
    {
    //:params user_id,product_id
        $meta_index = "reservation";
        $parameters = [];
        $count = 0;
        $data['where'] = [
            [
                "target" => "user_id",
                "operator" => "=",
                "value" => auth()->user()->id,
            ],
            [
                "target" => "owner_id",
                "operator" => "!=",
                "value" => null,
            ],

            [
                "target" => "deal",
                "operator" => "=",
                "value" => null,
            ],
        ];
        $data['relations'] = ["products","users"];
       

        $result = $this->fetchGeneric($data, $this->reservation);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No reservation found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved product - > reservation",
            "description" => "parameter either with user_id | post method",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    }

    public function push()
    {
        $payload = array(
			'to' => 'ExponentPushToken[VdLQTvAUYW4wou9NMMtuBh]',
			'sound' => 'default',
			'body' => 'Sample',
			'title'=> 'BOBONESS',
			'data' => (object) array('data' => 'foo'),
		);

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://exp.host/--/api/v2/push/send",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($payload),
			CURLOPT_HTTPHEADER => array(
				"Accept: application/json",
				"Accept-Encoding: gzip, deflate",
				"Content-Type: application/json",
				"cache-control: no-cache",
				"host: exp.host"
			),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			echo "cURL Error #:" . $err;
			} else {
			echo $response;
			}
    }
    

    public function approval($data = [])
    {
    //:params user_id,product_id
        $meta_index = "approval";
        $parameters = [];
        $count = 0;


        if(!isset($data['user_id']))
        {
            $data['relations'] = ["approval_product","approval_user"];

            $data['where'] = [
                [
                    "target" => "owner_id",
                    "operator" => "=",
                    "value" => auth()->user()->id,
                ],
            ];

        }elseif(isset($data['user_id']))
        {
            $data['relations'] = ["approval_product","approval_user"];

            $data['where'] = [
                [
                    "target" => "owner_id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
                [
                    "target" => "deal",
                    "operator" => "=",
                    "value" => null,
                ]

            ];
        }


         $result = $this->fetchGeneric($data, $this->reservation);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Users are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved approval",
            "description" => "parameter either with post method",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    }
    //follower ikaw is user_id
    public function followers($data = [])
    {
    //:params user_id,product_id
        $meta_index = "follow";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["Followersid"];
        if(!isset($data['user_id']))
        {

            $data['wherehas'][] = [
                'relation' => 'follow',
                'target' => [
                    [
                        'column' => "user_id",
                        "operator" => "=",
                        "value" => auth()->user()->id,
                    ],
                ],



            ];


        }elseif(isset($data['user_id']))
        {

            $data['wherehas'][] = [
                'relation' => 'follows',
                'target' => [
                    [
                        'column' => "user_id",
                        "operator" => "=",
                        "value" => $data['user_id'],
                    ],
                ],
            ];
        }


         $result = $this->fetchGeneric($data, $this->follow);



        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Followers are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved Followers",
            "description" => "parameter either with post method",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    }
    //you follow ikaw si follwer
    public function you_follow($data = [])
    {
    //:params user_id,product_id
        $meta_index = "follow";
        $parameters = [];
        $count = 0;
        $data['relations'] = ["Usersid"];
       if(isset($data['user_id']))
        {

            $data['wherehas'][] = [
                'relation' => 'follows',
                'target' => [
                    [
                        'column' => "follower",
                        "operator" => "=",
                        "value" => $data['user_id'],
                    ],
                ],
            ];
            }else{
                $data['wherehas'][] = [
                    'relation' => 'follows',
                    'target' => [
                        [
                            'column' => "follower",
                            "operator" => "=",
                            "value" => auth()->user()->id,
                        ],
                    ],
                ];
            }


         $result = $this->fetchGeneric($data, $this->follow);



        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Followers are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved You Follow",
            "description" => "parameter either with post method",
            "meta" => [
                $meta_index => $result,
            ],

        ]);

    }

    public function hasproduct($data = [])
    {
        $meta_index = "productexist";
        $parameters = [];
        $result = $userObj = Product::where('user_id', auth()->user()->id)->count();
        if($result>0)
        {
            $result = true;
        }
        else{
            $result = false;
        }
            return $this->setResponse([
                "code" => 200,
                "title" => "Successfully Retrieve Has Product",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
    }

    public function messages($data = [])
    {
        $meta_index = "users";
        $messObj = MessagesModel::all();
        $userObj = Users::all()->where('id',$data['user_id']);
        $result = [];
        foreach ($userObj as $key => $value) {
                    $user_id = $value->id;
                    array_push($result, "users");
                    array_push($result, $value);
                    foreach ($messObj as $key => $value2) {
                      if($value2->user_id == $user_id )
                      {
                        $prod_id = $value2->id;
                        array_push($result, "messages");
                        array_push($result, $value2);
                     }
            }
        }

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Users are found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved users->product->category",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);
    }


public function getNotif($data = [])
{
    $meta_index = "notif";
    $parameters = [];
    $count = 0;

    if (isset($data['user_id']) &&
        is_numeric($data['user_id'])) {

        $meta_index = "users";
        $data['single'] = true;
        $data['where'] = [
            [
                "target" => "seller_id",
                "operator" => "=",
                "value" => $data['user_id'],
            ],
        ];

        $parameters['user_id'] = $data['user_id'];

    }

    $count_data = $data;

    // $data['relations'][] = 'info';

    $result = $this->fetchGeneric($data, $this->mergenotif);

    if (!$result) {
        return $this->setResponse([
            'code' => 404,
            'title' => "No agents are found",
            "meta" => [
                $meta_index => $result,
            ],
            "parameters" => $parameters,
        ]);
    }

    // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

    return $this->setResponse([
        "code" => 200,
        "title" => "Successfully retrieved agents",
        "meta" => [
            $meta_index => $result,
            // "count" => $count,
        ],
        "parameters" => $parameters,
    ]);
}


//     public function getReviews($data = [])
//     {
//         $meta_index = "users";
//         $parameters = [];
//         $count = 0;
//         $data['order'] = "desc";
//         $data['sort'] = "created_at";
//         $data['relations'] = ["reviews"];

//         $data['single'] = true;
//         $data['where'] = [
//             [
//                 "target" => "id",
//                 "operator" => "=",
//                 "value" =>  auth()->user()->id,
//             ],
//         ];

//         $result = $this->fetchGeneric($data, $this->users);


//         if (!$result) {
//             return $this->setResponse([
//                 'code' => 404,
//                 'title' => "No reviews found",
//                 "meta" => [
//                     $meta_index => $result,
//                 ],
//             ]);
//         }
//         return $this->setResponse([
//             "code" => 200,
//             "title" => "Successfully reviews",
//             "description" => "UserInfo",
//             "meta" => [
//                 $meta_index => $result,
//             ],

//         ]);
//     }



    public function getUserRating($data = [])
    {
        $meta_index = "users";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['relations'] = ["userrating"];

        $data['single'] = true;
        if(!isset($data['user_id']))
        {
            $data['relations'] = ["userrating"];

            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => auth()->user()->id,
                ],
            ];

        }elseif(isset($data['user_id']))
        {
            $data['relations'] = ["userrating"];

            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
            ];
        }



        $result = $this->fetchGeneric($data, $this->users);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No reviews found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully reviews",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);
    }

    public function getUserInfo($data = [])
    {
        $meta_index = "users_information";
        $parameters = [];
        $count = 0;
        $data['order'] = "desc";
        $data['sort'] = "created_at";
        $data['relations'] = ["contact","location"];

        $data['single'] = true;
       
        if(isset($data['user_id']))
        {
        //     $data['relations'] = ["userrating"];

            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['user_id'],
                ],
            ];
        }



        $result = $this->fetchGeneric($data, $this->users);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No reviews found",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }
        return $this->setResponse([
            "code" => 200,
            "title" => "Success retrieve User Information",
            "description" => "UserInfo",
            "meta" => [
                $meta_index => $result,
            ],

        ]);
    }

public function getmessages($data = [])
    {
        $meta_index = "messages";
        $parameters = [];
        $count = 0;
        $data['single'] = false;
        $a = [];
        $a[0] = $data['sender_id'];
        $a[1] = $data['receiver_id'];
        $b[0] = $data['receiver_id'];
        $b[1] = $data['sender_id'];
        // $result = MessagesModel::orderBy('created_at', 'asc')->whereIn('receiver_id',$a)->whereIn('sender_id',$b)->get();

        $data['order'] = "asc";
        $data['sort'] = "created_at";
        // $data['relations'] = ["userrating"];

            // $data['relations'] = ["MassDelMessages"];

            $data['where'] = [
                [
                    "target" => "receiver_id",
                    "operator" => "=",
                    "value" => $a,
                ],
                [
                    "target" => "sender_id",
                    "operator" => "=",
                    "value" => $b,
                ]
            ];

            $result = $this->fetchGeneric($data, $this->messages);


        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No messages found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }
        // $count = $this->countData($count_data, refresh_model($this->messagess->getModel()));
        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved messages",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation

            if (!isset($data['name'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "Name is not set.",
                ]);
            }

            if (!isset($data['email'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "email is not set.",
                ]);
            }
            if (!isset($data['user_name'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "username is not set.",
                ]);
            }

            $data['password']= Hash::make($data['password']);


            $user = $this->users->init($this->users->pullFillable($data));
            $user->save($data);

            $data['user_id'] = $user->getUserid();
            if (!isset($data['image'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "image is not set.",
                ]);
            }else{
                $data['image_url']=null;
                    request()->validate([
                        'image' => 'required|image|mimes:jpeg,png,jpg|max:10000',

                    ]);
                    $imageName = time().'.'.request()->image->getClientOriginalExtension();
                    define('UPLOAD_DIR', 'storage/images/');
                    $file =  request()->image->move(UPLOAD_DIR,$imageName);
                    $url= asset($file);
                    $data['image_url'] = $url;

                    $image = $this->image->init($this->image->pullFillable($data));
                    $image->save($data);


            }


            $imageObj = $image::where('user_id',$user->getUserid())
            ->orderBy('id', 'desc')
            ->take(1)
            ->get('id');

            foreach ($imageObj as $key => $value) {
                $image_id = $value->id;
            }
            $data['image_id'] = $image_id;
                if (!isset($data['image_id'])) {
                    return $this->setResponse([
                        'code'  => 500,
                        'title' => "image id is not set.",
                    ]);
                }

            if (!$user->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $user->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully defined a user.",
                "parameters" => $user,
            ]);

    }

    

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }
        $user = $this->users->find($data['id']);
        if($user==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        if(isset($data["image"])){
                $url = $user->image_url;
                $file_name = basename($url);
                Storage::delete('images/' . $file_name);
                define('UPLOAD_DIR', 'storage/images/');
                $file = request()->image->move(UPLOAD_DIR, $data['image']);
                $url = asset($file);
                $data['image_url'] = $url;
        }

        $user->save($data);
        if (!$user->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $user->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a User.",
            "meta"        => [
                "status" => $user,
            ]
        ]);


    }


    public function deactivate($data = [])
    {

        $reason_int = $this->users->find($data['reason_int']);
        if($data['expected'] == "delete"){
            if (!$user->delete()) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $user->errors(),
                    ],
                ]);
            }
        }else;
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }
        if (!isset($data['mode'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "mode is not set.",
            ]);
        }
        if (!isset($data['reason'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reason is not set.",
            ]);
        }
        $user = $this->users->find($data['id']);
        if($user==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }
    
        

        $user->save($data);
        if (!$user->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $user->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a User.",
            "meta"        => [
                "status" => $user,
            ]
        ]);


    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $user = $this->users->find($data['id']);
        if($user==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "User not found.",
            ]);
        }

        if (!$user->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $user->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a User.",
            "meta"        => [
                "status" => $user,
            ]
        ]);


    }

    public function search($data = [])
    {

        $meta_index = "search";
        $parameters = [];
        $count = 0;




        $someVariable = $data['search'];




        $result = DB::select( DB::raw("SELECT DISTINCT p.`id` , p.`name` , p.`created_at` ,
        p.`donate` , p.`product_lat`, p.`location` FROM  product p JOIN delivery d ON p.`id` = d.`product_id` JOIN reservation r ON p.`id` = r.`product_id`
       WHERE p.`name` LIKE '%$someVariable%'
       OR p.`created_at`  LIKE '%$someVariable%'
       OR p.`product_lng`  LIKE '%$someVariable%'
       OR p.`product_lat`  LIKE '%$someVariable%'
       OR p.`location`  LIKE '%$someVariable%'
       OR p.`donate`  LIKE '%$someVariable%'

        "), array(
           'somevariable' => $someVariable
         ));




         if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "Search Not Found!",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->users->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Search Data",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);

    }

    public function forgot($data = [])
    {
        $password = str_random(8);
        $details = [
            'title' => 'Mail from ItSolutionStuff.com',
            'body' => 'Your new password is '.$password
        ];

         \Mail::to($data['email'])->send(new \App\Mail\MyTestMail($details));


          $user = User::where('email',$data['email'])->first();

          if ($user) {
              User::find($user->id)->update(['password' => Hash::make($password)]);
          }else{
            return $this->setResponse([
                "code" => 404,
                "title" => "No User Found"
            ]);
          }
            return $this->setResponse([
                    "code" => 200,
                    "title" => "Success message sent",
                ]);
    }
	
  public function emailChecker($data = [])
    {
        $point= DB::table('users')->where(

            'email', '=', $data['email']
        )->get();

        if($point->count() > 0){
            return $this->setResponse([
                "code" => 401,
                "title" => "Already Exist",
            ]);
        }
        return $this->setResponse([
                    "code" => 200,
                    "title" => "Available",
                ]);
    }


    // public function getContact($data = [])
    // {
    //     if (!isset($data['user_id'])) {
    //         return $this->setResponse([
    //             'code'  => 500,
    //             'title' => "user_id is not set.",
    //         ]);
    //     }   
    //     if (!isset($data['product_id'])) {
    //         return $this->setResponse([
    //             'code'  => 500,
    //             'title' => "product_id is not set.",
    //         ]);
    //     }  
    //     $meta_index = "product";
    //     $parameters = [];
    //     $count = 0;
    //     $data['order'] = "desc";
    //     $data['sort'] = "created_at";
    //     $data['relations'] = ["Location" , "products"];

    //     $data['single'] = true;
    //     if (isset($data['product_id'])) {
    //     $data['where'] = [
    //         [
    //             "target" => "id",
    //             "operator" => "=",
    //             "value" => $data['product_id'],
    //         ]
    //     ];
    // }
    //     $result = $this->fetchGeneric($data, $this->contact);


    //     if($result == null){
    //         return $this->setResponse([
    //             "code" => 401,
    //             "title" => "No Contact Information Available",
    //         ]);
    //     }
    //     return $this->setResponse([
    //                 "code" => 200,
    //                 "title" => "Successfully retreive",
    //                 "meta"        => [
    //                     "contact" => $result,
    //                 ]
    //             ]);
    // }

    public function setContactApproval($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }   
       
        $user = $this->users->find($data['id']);
        if($user==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "User not found.",
            ]);
        }
        $data['approval_contact'] = 1;
        $approval_Contact = $data['approval_contact'];
        $user->save($data);
        if (!$user->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $user->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully Approved Contact.",
            "meta"        => [
                "status" => $user,
            ]
        ]);

    }


    public function getApprovedRequested($data = [])
    {
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }   
        if (!isset($data['product_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "product_id is not set.",
            ]);
        }  

        if (!isset($data['request_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "request_id is not set.",
            ]);
        }  
        $meta_index = "contact";
        $parameters = [];
        $count = 0;


        $approvedRequest  = DB::table('request_contact')
        ->where('request_contact.user_id', '=',  $data['user_id'])
        ->where('request_contact.request_id', '=',  $data['request_id'])
        ->where('request_contact.product_id', '=',  $data['product_id'])
        ->where('request_contact.owner_approved', '!=', null)
        ->get()->count();

        $waiting  = DB::table('request_contact')
        ->where('request_contact.user_id', '=',  $data['user_id'])
        ->where('request_contact.request_id', '=',  $data['request_id'])
        ->where('request_contact.product_id', '=',  $data['product_id'])
        ->where('request_contact.owner_approved', '=', null)
        ->get()->count();


        $result  = DB::table('request_contact')
        ->where('request_contact.user_id', '=',  $data['user_id'])
        ->where('request_contact.request_id', '=',  $data['request_id'])
        ->where('request_contact.product_id', '=',  $data['product_id'])
        ->where('request_contact.owner_approved', '!=', null)
        ->get();


        if($approvedRequest==1){
            return $this->setResponse([
                'code'  => 201,
                'title' => "Already Approved",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }

        if($waiting==1){
            return $this->setResponse([
                'code'  => 201,
                'title' => "Waiting",
                "meta" => [
                    $meta_index => $result,
                ],
            ]);
        }


        $result = $this->fetchGeneric($data, $this->requestContact);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                 "count" => $this->requestContact::where('user_id','=',$data['user_id'])->
                 where('request_id','=',$data['request_id'])
                 ->
                 where('product_id','=',$data['product_id'])
                 ->get()->count()
            ],
            "parameters" => $parameters,
        ]);
    }



    




}
