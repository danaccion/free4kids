<?php

namespace App\Data\Repositories;

use App\Data\Models\followModel;
use App\Data\Repositories\BaseRepository;
use App\follow;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use DB;
class followRepository extends BaseRepository
{

    protected $follow;

    public function __construct(followModel $follow) 
    {
        $this->follow = $follow;
    }

    public function fetchfollow($data = [])
    {
        $meta_index = "follow";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "follow";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->follow);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No Images are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        
        if (!isset($data['status'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "status is not set.",
            ]);
        }

        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }
        
        // if (!isset($data['follower'])) {
        //     return $this->setResponse([
        //         'code'  => 500,
        //         'title' => "follower is not set.",
        //     ]);
        // }

        $data['follower'] = auth()->user()->id;
        $res = DB::table('follow')
        ->where('follower', '=', auth()->user()->id )
        ->where('user_id', '=',$data['user_id']);
        if($res->count() < 1 )   {
            $follow = $this->follow->init($this->follow->pullFillable($data));
            $follow->save($data);
        }
        else{
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Already follow",
            ]);
        }
            if (!$follow->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $follow->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully insert follow.",
                "parameters" => $follow,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['status'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "status is not set.",
            ]);
        }

        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }
        
        if (!isset($data['follower'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "follower is not set.",
            ]);
        }

        $follow = $this->follow->find($data['id']);
        if($follow==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $follow->save($data);
        if (!$follow->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $follow->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a follow.",
            "meta"        => [
                "status" => $follow,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $follow = $this->follow->find($data['id']);
        if($follow==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$follow->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $follow->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a follow.",
            "meta"        => [
                "status" => $follow,
            ]
        ]);
            
        
    }


}
