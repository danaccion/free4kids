<?php

namespace App\Data\Repositories;

use App\Data\Models\ReviewsModel;
use App\Data\Repositories\BaseRepository;
use App\Reviews;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ReviewsRepository extends BaseRepository
{

    protected $review;

    public function __construct(ReviewsModel $review) 
    {
        $this->review = $review;
    }

    public function fetchReviews($data = [])
    {
        $meta_index = "reviews";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "reviews";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->review);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        
       $data['user_id'] = auth()->user()->id;
            if (!isset($data['user_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "user_id is not set.",
                ]);
            }
        
            if (!isset($data['reviews'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "reviews is not set.",
                ]);
            }

            if (!isset($data['product_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "reviews is not set.",
                ]);
            }
    
            $review = $this->review->init($this->review->pullFillable($data));
            $review->save($data);

            if (!$review->save($data)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $review->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully create a review.",
                "parameters" => $review,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }
    
        if (!isset($data['user_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "user_id is not set.",
            ]);
        }
    
        if (!isset($data['reviews'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reviews is not set.",
            ]);
        }
        $review = $this->review->find($data['id']);
        
        $review->save($data);
        if (!$review->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $review->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a review.",
            "meta"        => [
                "status" => $review,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $review = $this->review->find($data['id']);
        if($review==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$review->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $review->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a review.",
            "meta"        => [
                "status" => $review,
            ]
        ]);
            
        
    }


}
