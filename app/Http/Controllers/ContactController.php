<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Data\Repositories\ContactRepository;

class ContactController extends BaseController
{
   protected $contact;

    public function __construct(
        ContactRepository $contact
    ){
        $this->contact = $contact;
    }

    public function create(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->contact->create($data))->json();
    }
    
   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
