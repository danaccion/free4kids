<?php



namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Data\Repositories\UsersRepository;
use App\Data\Repositories\ProductRepository;
use App\Data\Models\Users;
use App\Data\Models\Product;

use App\Data\Repositories\SpecificSearchRepository;
use App\Data\Models\SpecificSearchModel;


class SpecificSearchController extends BaseController
{
    protected $specific_search;

    public function __construct(
        SpecificSearchRepository $specific_search
    ){
        $this->specific_search = $specific_search;
    }
    public function index(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->specific_search->fetchspecific_search($data))->json();
    }

    public function create(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->specific_search->create($data))->json();
    }
    
    public function update(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->specific_search->update($data))->json();
    }

    public function delete(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->specific_search->delete($data))->json();
    }
    
    // public function search(Request $request)
    // {
       
    //     $data = $request->all();
    //     return $this->absorb($this->action_logs->search($data))->json();
    // }
    // /** 
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */

    // public function create(Request $request)
    // {
    //     $data = $request->all();
    //     return $this->absorb($this->action_logs->logsInputCheck($data))->json();     
    // }

    
    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function log(Request $request, $id)
    // {
    //     $data['id'] = $id;

    //     if (!isset($data['id']) ||
    //         !is_numeric($data['id']) ||
    //         $data['id'] <= 0) {
    //         return $this->setResponse([
    //             'code'  => 500,
    //             'title' => "Schedule ID is invalid.",
    //         ]);
    //     }

    //     return $this->absorb($this->action_logs->fetchUserLog($data))->json();
    // }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
