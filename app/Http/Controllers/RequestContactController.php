<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Data\Repositories\RequestContactRepository;

class RequestContactController extends BaseController
{
   protected $users;

    public function __construct(
        RequestContactRepository $requestContact
    ){
        $this->requestContact = $requestContact;
    }

    public function getContactRequest(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->requestContact->getContactRequest($data))->json();
    }

    public function create(Request $request)
    {
        $data = $request->all();
        return $this->absorb($this->requestContact->create($data))->json();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
}
