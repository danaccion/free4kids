<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
class Product extends Model
{
    use Notifiable, HasApiTokens;
    protected $primaryKey = 'user_id';
    protected $table = 'product';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id','image_url', 'location_lat','location_long','messages','approval_id','sender','reciever','user_id','age','sex','brand','stand','description'
    ];

    protected $hidden = [
        'expiry_date' 
    ];

}
